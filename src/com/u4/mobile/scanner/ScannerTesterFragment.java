package com.u4.mobile.scanner;

import org.androidannotations.annotations.*;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.u4.mobile.base.ui.FragmentBase;
import com.u4.mobile.scanner.Scanner.Scanable;

@EFragment(resName = "adm_scanner_tester__fragment")
public class ScannerTesterFragment extends FragmentBase implements Scanable {

	@ViewById(resName = "core_adm_scanner_tester__logo")
	ImageView logoButton;

	@ViewById(resName = "core_adm_scanner_tester__editbox_edit_text")
	EditText editEditText;

	@AfterViews
	public void setupView() {

		// Let's set the cursor at the end of any text in the editable text
		// field
		editEditText.setSelection(editEditText.getText().length());

		logoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// the image has been tapped so shoot off the intent to
				// DataWedge
				// to toggle the soft scan trigger
				// Create a new intent
				Intent i = new Intent();
				// set the intent action using soft scan trigger action string
				// declared earlier
				i.setAction(Scanner.ACTION_SOFTSCANTRIGGER);
				// add a string parameter to tell DW that we want to toggle the
				// soft scan trigger
				i.putExtra(Scanner.EXTRA_PARAM, Scanner.DWAPI_TOGGLE_SCANNING);
				// now broadcast the intent
				getActivity().sendBroadcast(i);
				// provide some feedback to the user that we did something
				Toast.makeText(v.getContext(), "Soft scan trigger toggled.", Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void onBarcodeScan(String data) {
		editEditText.setText(data);
		editEditText.setSelection(editEditText.getText().length());
	}
}
