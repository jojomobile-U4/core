package com.u4.mobile.scanner;

import android.content.Intent;

public final class Scanner {

	// Let's define some intent strings
	// This intent string contains the source of the data as a string
	public static final String SOURCE_TAG = "com.motorolasolutions.emdk.datawedge.source";
	// This intent string contains the barcode symbology as a string
	public static final String LABEL_TYPE_TAG = "com.motorolasolutions.emdk.datawedge.label_type";
	// This intent string contains the barcode data as a byte array list
	public static final String DECODE_DATA_TAG = "com.motorolasolutions.emdk.datawedge.decode_data";

	// This intent string contains the captured data as a string
	// (in the case of MSR this data string contains a concatenation of the
	// track data)
	public static final String DATA_STRING_TAG = "com.motorolasolutions.emdk.datawedge.data_string";

	// Let's define the MSR intent strings (in case we want to use these in the
	// future)
	public static final String MSR_DATA_TAG = "com.motorolasolutions.emdk.datawedge.msr_data";
	public static final String MSR_TRACK1_TAG = "com.motorolasolutions.emdk.datawedge.msr_track1";
	public static final String MSR_TRACK2_TAG = "com.motorolasolutions.emdk.datawedge.msr_track2";
	public static final String MSR_TRACK3_TAG = "com.motorolasolutions.emdk.datawedge.msr_track3";
	public static final String MSR_TRACK1_STATUS_TAG = "com.motorolasolutions.emdk.datawedge.msr_track1_status";
	public static final String MSR_TRACK2_STATUS_TAG = "com.motorolasolutions.emdk.datawedge.msr_track2_status";
	public static final String MSR_TRACK3_STATUS_TAG = "com.motorolasolutions.emdk.datawedge.msr_track3_status";

	// Let's define the API intent strings for the soft scan trigger
	public static final String ACTION_SOFTSCANTRIGGER = "com.motorolasolutions.emdk.datawedge.api.ACTION_SOFTSCANTRIGGER";
	public static final String EXTRA_PARAM = "com.motorolasolutions.emdk.datawedge.api.EXTRA_PARAMETER";
	public static final String DWAPI_START_SCANNING = "START_SCANNING";
	public static final String DWAPI_STOP_SCANNING = "STOP_SCANNING";
	public static final String DWAPI_TOGGLE_SCANNING = "TOGGLE_SCANNING";

	static String ourIntentAction = "com.u4.mobile.motorola.RECVR";
	
	public interface Scanable {
		
		void onBarcodeScan(String data);
	}
	

	// This function is responsible for getting the data from the intent
	// formatting it and adding it to the end of the edit box
	public static String handleDecodeData(Intent i) {

		// check the intent action is for us
		if (i.getAction() != null && i.getAction().contentEquals(Scanner.ourIntentAction)) {
			// define a string that will hold our output
			String source = i.getStringExtra(Scanner.SOURCE_TAG);

			// save it to use later
			if (source == null)
				source = "scanner";

			// get the data from the intent
			String data = i.getStringExtra(Scanner.DATA_STRING_TAG);

			// check if the data has come from the barcode scanner
			if (source.equalsIgnoreCase("scanner")) {
				// check if there is anything in the data
				if (data != null && data.length() > 0) {

					return data;
				}
			}
		}
		return null;
	}
}
