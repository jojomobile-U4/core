package com.u4.mobile.scanner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.StringUtils;

import android.widget.EditText;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.DialogFragmentBase;

@EFragment(resName = "enter_barcode_dialog")
public class EnterBarcodeDialog extends DialogFragmentBase {

	private OnEnterBarcodeListener listener;

	@ViewById(resName = "enter_barcode_dialog__barcode")
	public EditText barcodeEditText;

	@Click(resName = "enter_barcode_dialog__cancel_button")
	public void buttonCancelClick() {
		dismiss();
	}

	@Click(resName = "enter_barcode_dialog__ok_button")
	public void buttonEnterBarcodeClick() {
		if (!StringUtils.hasText(barcodeEditText.getText())) {
			barcodeEditText.setError(getResources().getString(R.string.app_required_field));

			return;
		}

		dismiss();

		if (listener != null) {
			listener.onEnterBarcode(barcodeEditText.getText().toString());
		}
	}

	@AfterViews
	public void populateView() {
		getDialog().setTitle(R.string.enter_barcode_dialog__title);
	}

	public void setOnEnterBarcodeListener(OnEnterBarcodeListener listener) {
		this.listener = listener;
	}

	public static interface OnEnterBarcodeListener {
		public void onEnterBarcode(String data);
	}
}
