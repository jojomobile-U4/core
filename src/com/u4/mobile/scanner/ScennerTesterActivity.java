package com.u4.mobile.scanner;

import org.androidannotations.annotations.*;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.NavigationDrawerActivity;

@EActivity
public class ScennerTesterActivity extends NavigationDrawerActivity {

	Fragment fragment;
	
	@Override
	public void poplutateView() {
		super.poplutateView();

		fragment = ScannerTesterFragment_.builder().build();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.navigation_drawer_frame_container, fragment).commit();
	}
}
