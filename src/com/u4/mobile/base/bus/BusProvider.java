package com.u4.mobile.base.bus;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.UiThread;

import com.squareup.otto.Bus;

@EBean(scope = Scope.Singleton)
public class BusProvider extends Bus {

	public BusProvider() {
		super();
	}

	@Override
	public void post(Object event) {
		postEvent(event);
	}

	@UiThread
	protected void postEvent(Object event) {
		super.post(event);
	}

	@Override
	public void register(Object event) {
		registerevent(event);
	}

	@UiThread
	protected void registerevent(Object event) {
		super.register(event);
	}

	@Override
	public void unregister(Object event) {
		unregisterevent(event);
	}

	@UiThread
	protected void unregisterevent(Object event) {
		super.unregister(event);
	}
}
