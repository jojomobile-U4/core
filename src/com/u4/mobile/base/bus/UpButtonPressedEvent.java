package com.u4.mobile.base.bus;

public class UpButtonPressedEvent {
	private int mActivityId;

	public UpButtonPressedEvent(int activityId) {
		mActivityId = activityId;
	}

	public int getActivityId() {
		return mActivityId;
	}

}
