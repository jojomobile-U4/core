package com.u4.mobile.base.helpers;

import com.u4.mobile.R;
import com.u4.mobile.base.ui.DialogFragmentBase;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.support.v4.app.DialogFragment;

public class DialogFactory {
	public static DialogFragment createDialogFragment(final Dialog dialog) {
		DialogFragmentBase df = new DialogFragmentBase();
		df.setDialog(dialog);
		return df;
	}

	public static DialogFragment createStandardDialog(final Context context, final int iconId, final int titleId, final String message) {
		return createDialogFragment(new AlertDialog.Builder(context).setIcon(iconId).setTitle(titleId).setMessage(message).setPositiveButton(R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}).create());
	}

	public static DialogFragment createStandardDialog(final Context context, final Drawable icon, final String title, final String message) {
		return createDialogFragment(new AlertDialog.Builder(context).setIcon(icon).setTitle(title).setMessage(message).setPositiveButton(R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}).create());
	}

	public static DialogFragment createInfo(final Context context, String message) {
		return createStandardDialog(context, android.R.drawable.ic_dialog_info, R.string.alert_dialog__information__title, message);
	}

	public static DialogFragment createWarning(final Context context, String message) {
		return createStandardDialog(context, android.R.drawable.ic_dialog_alert, R.string.alert_dialog__warning__title, message);
	}

	public static DialogFragment createError(final Context context, String message) {
		return createStandardDialog(context, android.R.drawable.ic_dialog_alert, R.string.alert_dialog__error__title, message);
	}

}
