package com.u4.mobile.base.helpers;

import org.androidannotations.annotations.EBean;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.conn.HttpHostConnectException;
import org.springframework.web.client.HttpClientErrorException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.u4.mobile.R;
import com.u4.mobile.base.app.ApplicationEnums.SyncDirection;
import com.u4.mobile.base.app.MainApp;

@EBean
public class NetworkHelper {

	public static boolean checkInternet() {
		Context context = MainApp.getInstance().getApplicationContext();
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}

	public static String translateMessage(Exception e) {
		return translateMessage(e, null);
	}

	public static String translateMessage(Exception e, SyncDirection syncDirection) {

		if (e == null) {
			return null;
		}

		Context context = MainApp.getInstance().getApplicationContext();

		if (e.toString().contains("java.net.UnknownHostException")) {
			return context.getResources().getString(R.string.error_message__unknown_host_exception);
		} else if (e.toString().contains("java.net.SocketTimeoutException")) {
			return context.getResources().getString(R.string.error_message__socket_time_exception);
		} else if (e.toString().contains("ConnectTimeoutException")) {
			return context.getResources().getString(R.string.error_message__timeout_exception);
		} else if (e.toString().contains("java.net.ssl.SSLException")) {
			return context.getResources().getString(R.string.error_message__connection_closed_by_peer);
		} else if (e.toString().contains("401 Unauthorized")) {
			return context.getResources().getString(R.string.error_message__unauthorized);
		} else if (e.toString().contains("412 Precondition Failed")) {
			if (syncDirection == SyncDirection.Download) {
				return context.getResources().getString(R.string.error_message__download_data_precondition_failed);
			} else if (syncDirection == SyncDirection.Upload) {
				return context.getResources().getString(R.string.error_message__upload_data_precondition_failed);
			} else if (syncDirection == SyncDirection.Both) {
				return context.getResources().getString(R.string.error_message__synchronization_data_precondition_failed);
			} else {
				return context.getResources().getString(R.string.error_message__precondition_failed);
			}
		} else if (e.toString().contains("500 Internal Server Error")) {
			return context.getResources().getString(R.string.error_message__internal_error);
		} else if(e.toString().contains("504 Gateway Timeout")){
			return context.getResources().getString(R.string.error_message__gateway_timeout);
		}

		if (e.getClass().getName().equalsIgnoreCase("org.springframework.web.client.HttpClientErrorException")) {
			final HttpClientErrorException springError = (HttpClientErrorException) e;
			if (springError.getStatusCode().name().equalsIgnoreCase(org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE.name())) {
				return context.getResources().getString(R.string.http_status__unsupported_media_type);
			} else if (springError.getStatusCode().name().equalsIgnoreCase(org.springframework.http.HttpStatus.NOT_ACCEPTABLE.name())) {
				return context.getResources().getString(R.string.http_status__not_acceptable);
			} else if (springError.getStatusCode().name().equalsIgnoreCase(org.springframework.http.HttpStatus.PAYMENT_REQUIRED.name())) {
				return context.getResources().getString(R.string.http_status__payment_required);
			} else if (springError.getStatusCode().name().equalsIgnoreCase(org.springframework.http.HttpStatus.FORBIDDEN.name())) {
				return context.getResources().getString(R.string.http_status__forbidden);
			} else if (springError.getStatusCode().name().equalsIgnoreCase(org.springframework.http.HttpStatus.NOT_FOUND.name())) {
				return context.getResources().getString(R.string.http_status__not_found);
			}
		} else if (e.getClass().getName().equalsIgnoreCase("org.springframework.web.client.ResourceAccessException")) {
			if(!checkInternet() || e.getCause() instanceof HttpHostConnectException){
				if(syncDirection != null){
					return context.getResources().getString(R.string.network__synchronization__no_connection_to_internet);					
				}
				
				return context.getResources().getString(R.string.network__no_connection_to_internet);
			} else {
				return context.getResources().getString(R.string.error_message__socket_time_exception);
			}
		}

		return e.toString();
	}

	public static String translateMessage(HttpResponse response, String objectName) {

		if (response == null) {
			return null;
		}

		Context context = MainApp.getInstance().getApplicationContext();

		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_GATEWAY_TIMEOUT) {
			return context.getResources().getString(R.string.http_status__sc_gateway_timeout);
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			return context.getResources().getString(R.string.http_status__sc_unauthorized);
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND) {
			return String.format(context.getResources().getString(R.string.http_status__sc_not_found), objectName);
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_BAD_REQUEST) {
			return String.format(context.getResources().getString(R.string.http_status__sc_bad_request), objectName);
		}

		return response.getStatusLine().toString();
	}
}
