package com.u4.mobile.base.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.Field;
import java.sql.SQLException;

import org.androidannotations.annotations.*;
import org.androidannotations.annotations.sharedpreferences.Pref;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.base.services.RestHelperBase;
import com.u4.mobile.base.ui.ErrorDialogFragment;
import com.u4.mobile.model.DatabaseHelper;
import com.u4.mobile.model.DatabaseHolder;
import com.u4.mobile.model.SessionInfo;
import com.u4.mobile.model.User;
import com.u4.mobile.utils.LogHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

@EApplication
public class MainApp extends Application {
	private static final String CRASH_FILE = "crash-report";
	private static MainApp instance;
	private String loginName;
	private String pass;
	private String crashFilePath;
	private SessionInfo sessionInfo;

	@Bean
	public RestHelperBase restHelper;

	@Pref
	public AppSettings_ appSettings;

	@OrmLiteDao(helper = DatabaseHelper.class, model = User.class)
	public Dao<User, Long> userDao;

	public MainApp() {
		instance = this;
	}

	public static MainApp getInstance() {
		return instance;
	}

	public String getCrashFilePath() {
		return crashFilePath;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		crashFilePath = getFilesDir().getAbsolutePath() + CRASH_FILE;
		Thread.setDefaultUncaughtExceptionHandler(new DefaultUncaughtExceptionHandler());
	}

	public String getLogin() {
		return loginName;
	}

	public String getPassword() {
		return pass;
	}

	public static boolean isGoogleMapsApiAvailable() {
		try {
			Class.forName("com.google.android.maps.MapActivity");
			return true;
		} catch (final Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public void setLogin(String login) {
		loginName = login;
	}

	public void setPassword(String password) {
		pass = password;
	}
	
	public Version getAppVersion() {
		Version version = null;
		
		try {
			version = new Version(getPackageManager().getPackageInfo(instance.getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}
		
		return version;
	}

	class DefaultUncaughtExceptionHandler implements UncaughtExceptionHandler {
		private String getBuildInfo() {
			final StringBuilder sb = new StringBuilder();
			final Field[] declaredFields = android.os.Build.class.getDeclaredFields();
			
			for (final Field field : declaredFields) {
				if (java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
					sb.append(field.getName());
					sb.append(": ");
					try {
						sb.append(field.get(null).toString());
					} catch (final Exception e) {
					}
					sb.append("\n");
				}
			}
			
			return sb.toString();
		}

		@Override
		public void uncaughtException(Thread thread, Throwable throwable) {
			throwable.printStackTrace();
			final String crashFile = getCrashFilePath();
			
			try {
				final FileOutputStream out = new FileOutputStream(crashFile);
				try {
					if (throwable.getLocalizedMessage() != null) {
						out.write(throwable.getLocalizedMessage().getBytes());
					}
					out.write("\n\n".getBytes());
					out.write(Log.getStackTraceString(throwable).getBytes());
					out.write("\n\n".getBytes());
					out.write(getBuildInfo().getBytes());
				} finally {
					out.close();
				}
			} catch (final Exception e) {
				Log.e("uncaughtExceptionHanlder", e.toString());
			}

			Log.e("logged uncaught exception", throwable.getMessage(), throwable);
			showErrorDialog(throwable.getMessage());

			// android.os.Process.killProcess(android.os.Process.myPid());
			// System.exit(10);
		}
	}

	public void showErrorDialog(String message) {
		new UnExeption().execute(message, null, null);
	}

	private class UnExeption extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
		}

		@Override
		protected void onPreExecute() {
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			new AlertDialog.Builder(getApplicationContext()).setMessage("Please provide a valid String").setTitle("HAT Alert !!!").setCancelable(true)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int whichButton) {
							return;
						}
					}).show();
			// Natomiast metoda onProgressUpdate umozliwia aktualizwanie watku
			// głównej podczas działana naszego UnExeption
		}
	}

	public void onClick(View v) {
		new UnExeption().execute("");
	}

	public void showCrashReport(final Activity context) {
		new ShowCrashReportAsyncTask(context).execute();
	}

	class ShowCrashReportAsyncTask extends AsyncTask<Void, Void, String> {
		private Activity context;

		public ShowCrashReportAsyncTask(Activity context) {
			this.context = context;
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				final File file = new File(getInstance().getCrashFilePath());
				
				if (file.exists()) {
					RandomAccessFile f = null;
					
					try {
						f = new RandomAccessFile(file, "r");
					} catch (final FileNotFoundException e) {
						Log.e(MainApp_.class.getName(), e.getMessage(), e);
					}
					
					if (f != null) {
						byte[] bytes = null;
						
						try {
							bytes = new byte[(int) f.length()];
							f.readFully(bytes);
						} catch (final IOException e) {
							Log.e(MainApp_.class.getName(), e.getMessage(), e);
						} finally {
							try {
								f.close();
							} catch (final IOException e) {
								Log.e(MainApp_.class.getName(), e.getMessage(), e);
							}
						}
						
						if (bytes != null) {
							return new String(bytes);
						}
					}
				}
			} catch (final Exception e) {
				Log.e("showCrashReport", e.toString());
				e.printStackTrace();
			}
			
			return null;
		}

		@Override
		public void onPostExecute(String message) {
			if (message == null || context == null || context.isFinishing()) {
				return;
			}
			
			final DialogFragment df = new ErrorDialogFragment();
			final FragmentTransaction ft = context.getFragmentManager().beginTransaction();
			final Fragment prev = context.getFragmentManager().findFragmentByTag("errorDialog");
			
			if (prev != null) {
				ft.remove(prev);
			}
			
			final Bundle args = new Bundle();
			args.putString(Intent.EXTRA_BUG_REPORT, message);
			df.setArguments(args);
			df.show(ft, "errorDialog");
		}
	}

	public void setSessionInfo(SessionInfo sessionInfo) {
		this.sessionInfo = sessionInfo;
	}

	public SessionInfo getSessionInfo() {
		if (sessionInfo == null) {
			initializeSessionInfo();
		}
		
		return sessionInfo;
	}

	public void clearSessionInfo() {
		sessionInfo = null;
	}
	
	public void initializeSessionInfo() {
		if (sessionInfo == null || !sessionInfo.getUserName().equalsIgnoreCase(appSettings.lastUserName().get())) {
			if (DatabaseHolder.checkDatabase(this)) {
				QueryBuilder<User, Long> queryBuilder = userDao.queryBuilder();

				try {
					queryBuilder.where().eq(User.NAME__FIELD_NAME, appSettings.lastUserName().get().toUpperCase());
					User user = queryBuilder.queryForFirst();

					if (user != null) {
						sessionInfo = new SessionInfo(user);
					}
				} catch (SQLException e) {
					LogHelper.setErrorMessage(this.getClass().getName(), e);
				}
			}
		}
	}
}