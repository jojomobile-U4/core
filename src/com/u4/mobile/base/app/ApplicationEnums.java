package com.u4.mobile.base.app;

public class ApplicationEnums {

	public enum Fonts {
		Roboto_ThinItalic, Roboto_Thin, Roboto_Regular, Roboto_MediumItalic, Roboto_Medium, Roboto_LightItalic, Roboto_Light, Roboto_Italic, Roboto_CondensedItalic, Roboto_Condensed, Roboto_BoldItalic, Roboto_BoldCondensedItalic, Roboto_BoldCondensed, Roboto_Bold, Roboto_BlackItalic, Roboto_Black
	}

	public enum Sex {
		Woman(0), Man(1);

		private int value;

		private Sex(int sex) {
			value = sex;
		}

		public int getValue() {
			return value;
		}
	}

	public enum MessageEquivalent {
		NoMessage("No message");

		private String value;

		private MessageEquivalent(String message) {
			value = message;
		}

		public String getValue() {
			return value;
		}
	}

	public enum SyncDirection {
		Download, Upload, Both
	}

	public enum RecordStates {
		NoChange(0), Inserted(1), Updated(2), Deleted(3);

		private final int value;

		RecordStates(int value) {

			this.value = value;
		}

		public Integer intValue() {

			return value;
		}
		
		public int getValue() {
			return value;
		}
	}

	public enum DialogResults {
		Cancel(0), Ok(1), Yes(2), No(3);

		private final int value;

		DialogResults(int value) {

			this.value = value;
		}

		public Integer intValue() {

			return value;
		}
	}

	public enum DialogRequests {
		Browse(0), Reload(1);

		private final int value;

		DialogRequests(int value) {

			this.value = value;
		}

		public Integer intValue() {

			return value;
		}
	}
	
	public enum SyncOperation {
		C, D
	}
}
