package com.u4.mobile.base.app;

import org.androidannotations.annotations.sharedpreferences.*;

@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface AppSettings {

	@DefaultString("https://")
	String serviceUrl();

	String userName();

	String userPassword();

	String lastUserName();

	String lastUserPassword();
	
	@DefaultBoolean(false)
	boolean hasChangedSettings();
	
	@DefaultBoolean(true)
	boolean saveCredentials();

	String proxyUrl();

	String serverCertificatePath();

	String clientCertificateAlias();

	@DefaultBoolean(false)
	boolean serverCertRequired();
}