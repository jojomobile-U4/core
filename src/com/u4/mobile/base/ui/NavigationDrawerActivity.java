package com.u4.mobile.base.ui;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.u4.mobile.R;

@EActivity
public abstract class NavigationDrawerActivity extends ActivityBase {
	private CharSequence title;
	private ActionBarDrawerToggle drawerToggle;
	private int contentViewId = -1;
	private int drawerListPosition = -99;

	@Bean
	protected NavigationDrawerListAdapter drawerListAdapter;

	@ViewById(resName = "navigation_drawer")
	protected DrawerLayout drawerLayout;

	@ViewById(resName = "navigation_drawer_list")
	protected ListView drawerList;

	public NavigationDrawerActivity() {
		this.contentViewId = -1;
	}

	public NavigationDrawerActivity(int contentViewId) {
		this.contentViewId = contentViewId;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.navigation_drawer_act);
	}

	@AfterViews
	public void onCreateView() {
		// Inject content view into drawer layout
		if (contentViewId > 0) {
			LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View contentView = inflater.inflate(contentViewId, drawerLayout, false);
			drawerLayout.addView(contentView, 0);
		}

		title = getTitle();

		drawerList.setOnItemClickListener(new SlideMenuClickListener());
		drawerList.setAdapter(drawerListAdapter);

		// Enabling action bar application icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, // Navigation
																							// menu
																							// toggle
																							// icon
				R.string.app_name, // Navigation drawer open - description for
									// accessibility
				R.string.app_name // Navigation drawer close - description for
									// accessibility
		) {
			@Override
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(title);
				// Calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(R.string.app_name);
				// Calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};

		drawerLayout.setDrawerListener(drawerToggle);

		loadViewData();
	}

	@Background
	public void loadViewData() {
		drawerListAdapter.loadData();
		poplutateView();
	}

	@UiThread
	public void poplutateView() {
		drawerListPosition = drawerListAdapter.getPosition(this.getClass());
		// Set drawer list position for current activity
		if (drawerListPosition > -1) {
			drawerList.setItemChecked(drawerListPosition, true);
			drawerList.setSelection(drawerListPosition);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		drawerListPosition = drawerListAdapter.getPosition(this.getClass());
		// Set drawer list position for current activity
		if (drawerListPosition > -1) {
			drawerList.setItemChecked(drawerListPosition, true);
			drawerList.setSelection(drawerListPosition);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Toggle navigation drawer on selecting action bar application
		// icon/title
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void setTitle(CharSequence title) {
		this.title = title;
		getActionBar().setTitle(this.title);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (drawerToggle != null) {
			drawerToggle.syncState();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		drawerToggle.onConfigurationChanged(newConfig);
	}

	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// Display view for selected navigation drawer item
			drawerLayout.closeDrawer(drawerList);
			startActivity(new Intent(getBaseContext(), drawerListAdapter.getItem(position).getActivityClass()));
		}
	}

	@UiThread
	public void showDialogInfo(String message) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(getResources().getString(R.string.app_name));
		alertDialog.setMessage(message);
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.ok), (DialogInterface.OnClickListener) null);
		alertDialog.show();
	}
}
