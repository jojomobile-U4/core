package com.u4.mobile.base.ui;

import java.util.List;

import org.androidannotations.annotations.EActivity;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.u4.mobile.R;

@EActivity
public abstract class NavigationDrawerTabActivity extends NavigationDrawerActivity {

	protected PagerAdapter pagerAdapter;
	protected ViewPager viewPager;

	public NavigationDrawerTabActivity() {
		super(R.layout.fragment_tab);
	}

	public abstract List<FragmentBase> getFragments();

	@Override
	public void poplutateView() {
		super.poplutateView();
		pagerAdapter = new PageSectionsPagerAdapter(getSupportFragmentManager(), getFragments());
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(pagerAdapter);
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				invalidateOptionsMenu();
			}
		});
	}
}
