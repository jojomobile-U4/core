package com.u4.mobile.base.ui;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.springframework.util.StringUtils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;

import com.u4.mobile.R;
import com.u4.mobile.base.app.ApplicationEnums;
import com.u4.mobile.scanner.Scanner;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.utils.LogHelper;

@EFragment
public class DialogFragmentBase extends DialogFragment implements Scanable {
	private static IntentFilter receiverFilter = new IntentFilter("com.u4.mobile.motorola.RECVR");
	private OnDismissListener onDismissListener;
	private Dialog dialog;

	private final BroadcastReceiver scannerReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			if (Scanable.class.isInstance(getDialog())) {
				((Scanable) getDialog()).onBarcodeScan(Scanner.handleDecodeData(intent));
			}
		}
	};

	public DialogFragmentBase() {
		super();
		setStyle(STYLE_NORMAL, R.style.DialogTheme);
	}

	@Override
	public void onBarcodeScan(String data) {}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		getActivity().registerReceiver(scannerReceiver, receiverFilter);

		if (dialog != null) {
			return dialog;
		}
		return super.onCreateDialog(savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			getActivity().unregisterReceiver(scannerReceiver);
		} catch (Exception e) {
			LogHelper.setErrorMessage("ActivityBase.unregisterReceiver(scannerReceiver)", e);
		}

	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		if (onDismissListener != null) {
			onDismissListener.onDismiss(dialog);
		}
	}

	public void setDialog(Dialog dialog) {
		this.dialog = dialog;
	}

	public void setOnDismissListener(OnDismissListener listener) {
		onDismissListener = listener;
	}

	@UiThread
	public void showDialogInfo(int resId) {
		showDialogInfo(getString(resId));
	}

	@UiThread
	public void showDialogInfo(String message) {

		if (!StringUtils.hasText(message)) {
			message = ApplicationEnums.MessageEquivalent.NoMessage.getValue();
		}

		AlertDialog alertDialog = new AlertDialog.Builder(this.getActivity()).create();
		alertDialog.setTitle(getResources().getString(R.string.app_name));
		alertDialog.setMessage(message);
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", (DialogInterface.OnClickListener) null);
		alertDialog.show();
	}

	protected boolean validateControl(EditText control) {
		if (!StringUtils.hasLength(control.getText().toString())) {
			control.setError(getResources().getString(R.string.app_required_field));
			return false;
		}

		return true;
	}
}
