package com.u4.mobile.base.ui;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;

import android.widget.AbsListView;
import android.widget.ListView;

import com.u4.mobile.model.DownloadTransferObjectBase;

@EFragment
public abstract class ListFragmentBase<T extends DownloadTransferObjectBase> extends FragmentBase {

	protected ListView listView;
	protected ListAdapterBase<T> adapter;
	protected Long listPageSize = 10L;
	private boolean isSetAllSelected = false;

	@UiThread
	public void reloadData(int selectPositionID) {
		adapter.reloadData(true, 0L);
		selectItem(selectPositionID);
	}

	@UiThread
	public void reloadData(long selectObjectID) {
		adapter.reloadData(true, 0L);
		selectItem(selectObjectID);
	}

	@UiThread
	public void selectItem(int position) {
		listView.setItemChecked(position, true);
	}

	@UiThread
	public void selectItem(long id) {
		listView.setItemChecked(getAdapterItemPosition(id), true);
	}

	protected void changeAllItemSelected() {
		isSetAllSelected = !isSetAllSelected;
		adapter.setAllItemsChecked(isSetAllSelected);
		listView.invalidate();
	}

	protected void getList() {
		adapter.init(this, listPageSize);
		listView.setAdapter(adapter);

		// TODO: �adowanie danych na onScroll przy wej�ciu na formatk� odbywa
		// sie trzykrotnie. Do zastanowienia jak to obej��.
		listView.setOnScrollListener(new AbsListView.OnScrollListener() {
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				if (firstVisibleItem + visibleItemCount >= totalItemCount && !adapter.isInProgress()) {
					if (adapter.hasMoreItems()) {
						adapter.getMoreData();
					}
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}
		});
	}

	protected boolean isSetAllSelected() {
		return isSetAllSelected;
	}

	private int getAdapterItemPosition(long id) {

		for (int position = 0; position < listView.getCount(); position++) {
			if (adapter.getItem(position).getId() == id) {
				return position;
			}
		}
		return 0;
	}

}
