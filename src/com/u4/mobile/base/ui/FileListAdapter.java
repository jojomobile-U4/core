package com.u4.mobile.base.ui;

import java.io.File;
import java.util.List;

import com.u4.mobile.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FileListAdapter extends ArrayAdapter<String> {

	public FileListAdapter(Context context, List<String> objects) {
		super(context, R.layout.file_list_view, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		try {
			if (convertView == null) {
				final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.file_item_list_view, null);
				holder = new ViewHolder(convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.populateFrom(getItem(position));

		} catch (final Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	class ViewHolder {
		TextView path;

		ViewHolder(View row) {
			path = (TextView) row.findViewById(R.id.rowtext);
		}

		void populateFrom(String value) {
			path.setText(value);
			if (value.endsWith(File.separator)) {
				path.setTypeface(path.getTypeface(), Typeface.BOLD);
				path.setTextSize(22);
			}
		}
	}
}
