package com.u4.mobile.base.ui;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

@EViewGroup(resName = "navigation_drawer_list_item_view")
public class NavigationDrawerListItemView extends RelativeLayout {

	@ViewById(resName = "navigation_drawer_list_item__icon")
	ImageView iconImageView;

	@ViewById(resName = "navigation_drawer_list_item__title")
	TextView titleTextView;

	@ViewById(resName = "navigation_drawer_list_item__counter")
	TextView counterTextView;

	public NavigationDrawerListItemView(Context context) {
		super(context);
	}

	public void populateView(final NavigationDrawerListItem listItem) {

		if (listItem.getIcon() > 0) {
			iconImageView.setImageResource(listItem.getIcon());
		}

		titleTextView.setText(listItem.getTitle());

		// displaying count
		// check whether it set visible or not
		if (listItem.getCounterVisibility()) {
			counterTextView.setText(listItem.getCount().toString());
		} else {
			// hide the counter view
			counterTextView.setVisibility(View.GONE);
		}
	}

}
