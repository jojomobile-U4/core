package com.u4.mobile.base.ui;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.StringUtils;

import android.content.res.Configuration;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import com.u4.mobile.R;

@EFragment(resName = "datetime_picker_dialog")
public class DateTimePickerDialog extends DialogFragmentBase {
	public static final String DATETIME_ARGUMENT = "dateTime";
	public static final String TITLE = "title";
	public static final String ONLY_DATE = "onlyDate";
	public static final String ONLY_TIME = "onlyTime";
	
	private OnDateTimeSetListener listener;
	
	@FragmentArg(DATETIME_ARGUMENT)
	public Date dateTime;

	@FragmentArg(TITLE)
	public String title;

	@FragmentArg(ONLY_DATE)
	public Boolean onlyDate;

	@FragmentArg(ONLY_TIME)
	public Boolean onlyTime;	

	@ViewById(resName = "datetime_picker__date_picker")
	public DatePicker datePicker;

	@ViewById(resName = "datetime_picker__time_picker")
	public TimePicker timePicker;

	@ViewById(resName = "datetime_picker__layout")
	public LinearLayout layout;

	public DateTimePickerDialog() {
		super();
	}

	@Click(resName = "datetime_picker__cancel_button")
	@Background
	public void buttonCancelClick() {
		dismiss();
	}

	@Click(resName = "datetime_picker__ok_button")
	public void buttonOkClick() {
		if (listener != null) {
			listener.onDateTimeSet(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());
			dismiss();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		layout.setOrientation(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE ? LinearLayout.HORIZONTAL : LinearLayout.VERTICAL);
	}

	@AfterViews
	public void populateView() {
		int year, monthOfYear, dayOfMonth, hour, minute;

		final Calendar calendar = new GregorianCalendar();
		
		if (dateTime != null) {
			calendar.setTime(dateTime);
		}

		year = calendar.get(Calendar.YEAR);
		monthOfYear = calendar.get(Calendar.MONTH);
		dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);

		datePicker.init(year, monthOfYear, dayOfMonth, null);

		timePicker.setIs24HourView(true);
		timePicker.setCurrentHour(hour);
		timePicker.setCurrentMinute(minute);

		// layout.setOrientation(getResources().getConfiguration().orientation
		// == Configuration.ORIENTATION_LANDSCAPE ? LinearLayout.HORIZONTAL :
		// LinearLayout.VERTICAL);

		if (onlyDate) {
			timePicker.setVisibility(View.GONE);
			getDialog().setTitle(R.string.datetime_picker__title_only_date);
		} else if (onlyTime) {
			datePicker.setVisibility(View.GONE);
			getDialog().setTitle(R.string.datetime_picker__title_only_time);
		} else {
			getDialog().setTitle(R.string.datetime_picker__title);
		}

		if (StringUtils.hasText(title)) {
			getDialog().setTitle(title);
		}
	}

	public void setOnDateTimeSetListener(OnDateTimeSetListener listener) {
		this.listener = listener;
	}

	public static interface OnDateTimeSetListener {
		public void onDateTimeSet(int year, int mont, int dayOfMonth, int hour, int minute);
	}
}