package com.u4.mobile.base.ui;

import com.u4.mobile.base.app.ApplicationEnums.Fonts;
import com.u4.mobile.utils.FontHelper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextView_Roboto extends TextView {

	public TextView_Roboto(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		// Typeface.createFromAsset doesn't work in the layout editor. Skipping...
		if (isInEditMode()) {
			return;
		}
		setTypeface(FontHelper.ParseFontAttributes(attrs));
	}

	public TextView_Roboto(Context context, AttributeSet attrs) {
		super(context, attrs);

		// Typeface.createFromAsset doesn't work in the layout editor. Skipping...
		if (isInEditMode()) {
			return;
		}

		setTypeface(FontHelper.ParseFontAttributes( attrs));
	}

	public TextView_Roboto(Context context) {
		super(context);

		// Typeface.createFromAsset doesn't work in the layout editor. Skipping...
		if (isInEditMode()) {
			return;
		}
		setTypeface(FontHelper.ParseFontAttributes(context, Fonts.Roboto_Bold));
	}

	@Override
	public void setTypeface(Typeface tf) {
		super.setTypeface(tf);
	}

	
}
