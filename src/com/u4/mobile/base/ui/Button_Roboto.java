package com.u4.mobile.base.ui;

import com.u4.mobile.base.app.ApplicationEnums.Fonts;
import com.u4.mobile.utils.FontHelper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class Button_Roboto extends Button {

	public Button_Roboto(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		if (isInEditMode()) {
			return;
		}
		setTypeface(FontHelper.ParseFontAttributes(attrs));
	}

	public Button_Roboto(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (isInEditMode()) {
			return;
		}

		setTypeface(FontHelper.ParseFontAttributes(attrs));
	}

	public Button_Roboto(Context context) {
		super(context);

		if (isInEditMode()) {
			return;
		}
		setTypeface(FontHelper.ParseFontAttributes(context, Fonts.Roboto_Bold));
	}

	@Override
	public void setTypeface(Typeface tf) {
		super.setTypeface(tf);
	}
}