package com.u4.mobile.base.ui;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.springframework.util.StringUtils;
import org.androidannotations.annotations.*;

import com.u4.mobile.R;
import com.u4.mobile.base.app.AppSettings;
import com.u4.mobile.utils.ApplicationCacheHelper;
import com.u4.mobile.utils.LogHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.security.KeyChain;
import android.security.KeyChainAliasCallback;
import android.util.Log;
import android.widget.Toast;

@EFragment
public class AppSettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener, OnPreferenceClickListener, KeyChainAliasCallback {

	public final int INSTALL_SERVER_CERT_FILE_VIEW_REQUEST_CODE = 1;
	public final int INSTALL_SERVER_CERT_INSTALL_DIALOG_REQUEST_CODE = 2;

	public final static String INSTALL_SERVER_CERT_PATH = "CertServerPath";
	public final static String INSTALL_SERVER_CERT_BUTTON = "install_server_cert_button";
	public final static String SHOW_SERVER_CERT_BUTTON = "show_server_cert_button";
	public final static String DELETE_SERVER_CERT_BUTTON = "delete_server_cert_button";

	public final static String INSTALL_CLIENT_CERT_PATH = "CertClientPath";
	public final static String INSTALL_CLIENT_CERT_BUTTON = "install_client_cert_button";
	public final static String SHOW_CLIENT_CERT_BUTTON = "show_client_cert_button";
	public final static String DELETE_CLIENT_CERT_BUTTON = "delete_client_cert_button";

	public final static String SERVER_CERT_REQUIRED = "serverCertRequired";
	public final static String CLIENT_CERTIFICATE_ALIAS = "clientCertificateAlias";
	public final static String SERVER_CERTIFICATE_PATH = "serverCertificatePath";

	public final static String HOME_BUTTON = "button_home_category_key";

	private PreferenceManager prefManager;

	private static Intent resultIntent = new Intent();
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		prefManager = getPreferenceManager();
		prefManager.setSharedPreferencesName(AppSettings.class.getSimpleName());

		addPreferencesFromResource(R.xml.preferences);

		findPreference(INSTALL_SERVER_CERT_BUTTON).setOnPreferenceClickListener(this);
		findPreference(SHOW_SERVER_CERT_BUTTON).setOnPreferenceClickListener(this);
		findPreference(DELETE_SERVER_CERT_BUTTON).setOnPreferenceClickListener(this);

		findPreference(INSTALL_CLIENT_CERT_BUTTON).setOnPreferenceClickListener(this);
		findPreference(SHOW_CLIENT_CERT_BUTTON).setOnPreferenceClickListener(this);
		findPreference(DELETE_CLIENT_CERT_BUTTON).setOnPreferenceClickListener(this);

		findPreference(HOME_BUTTON).setOnPreferenceClickListener(this);

		getActivity().setResult(Activity.RESULT_CANCELED, resultIntent);
	}

	
	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
		refreshServerCertificateState();
		refreshClientCertificateState();
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

		getActivity().setResult(Activity.RESULT_OK, resultIntent);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == INSTALL_SERVER_CERT_FILE_VIEW_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				String path = data.getExtras().getString(INSTALL_SERVER_CERT_PATH);

				try {
					installServerCertificate(path);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else if (requestCode == INSTALL_SERVER_CERT_INSTALL_DIALOG_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_CANCELED) {
			}
		}
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {

		String key = preference.getKey();
		if (key.equals(INSTALL_SERVER_CERT_BUTTON)) {
			installServerCertificate();
		} else if (key.equals(SHOW_SERVER_CERT_BUTTON)) {
			Intent intent = new Intent(getActivity(), CertificateActivity_.class);
			intent.putExtra(CLIENT_CERTIFICATE_ALIAS, false);
			startActivity(intent);
		} else if (key.equals(DELETE_SERVER_CERT_BUTTON)) {
			boolean result = deleteCertificate(SERVER_CERTIFICATE_PATH, R.string.pref_delete_server_cert_button_msg_question, R.string.pref_delete_server_cert_button_msg_result);			;
			CheckBoxPreference pref = (CheckBoxPreference) findPreference(SERVER_CERT_REQUIRED);
			pref.setChecked(false);
			return result;
		} else if (key.equals(INSTALL_CLIENT_CERT_BUTTON)) {
			chooseCert();
		} else if (key.equals(SHOW_CLIENT_CERT_BUTTON)) {
			Intent intent = new Intent(getActivity(), CertificateActivity_.class);
			intent.putExtra(CLIENT_CERTIFICATE_ALIAS, true);
			startActivity(intent);
		} else if (key.equals(DELETE_CLIENT_CERT_BUTTON)) {
			// TODO - prawdziwe usuwanie
			prefManager.getSharedPreferences().edit().putString(CLIENT_CERTIFICATE_ALIAS, null).commit();
			refreshClientCertificateState();
		} else if (key.equals(HOME_BUTTON)) {
			//TODO:startActivity(new Intent(getActivity(), HomeActivity_.class));
		}
		return false;
	}

	@Override
	public void alias(final String alias) {
		prefManager.getSharedPreferences().edit().putString(CLIENT_CERTIFICATE_ALIAS, alias).commit();

		if (StringUtils.hasText(alias)) {
			new InstallClientCertificate().execute(alias, null, null);
		}
	}

	private void installServerCertificate() {

		final String path = prefManager.getSharedPreferences().getString(SERVER_CERTIFICATE_PATH, null);

		if (StringUtils.hasText(path)) {
			new AlertDialog.Builder(getActivity()).setTitle(String.format(getString(R.string.pref_category_security))).setIcon(R.drawable.ic_launcher).setMessage(String.format(getString(R.string.pref_prepare_to_server_cert_install_question), StringUtils.getFilename(path)))
					.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							try {
								if (ApplicationCacheHelper.removeFile(path)) {
									Toast.makeText(getActivity(), getString(R.string.pref_delete_server_cert_button_msg_result), Toast.LENGTH_SHORT).show();
									prefManager.getSharedPreferences().edit().putString(SERVER_CERTIFICATE_PATH, null).commit();
									refreshServerCertificateState();
								}

								startActivityForResult(new Intent(getActivity(), FileListView_.class), INSTALL_SERVER_CERT_FILE_VIEW_REQUEST_CODE);

							} catch (IOException e) {
								LogHelper.setErrorMessage("AppSettingsFragment.prepareToServerCertificateInstall", e);
							}
						}
					}).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
						}
					}).show();
		} else {
			startActivityForResult(new Intent(getActivity(), FileListView_.class), INSTALL_SERVER_CERT_FILE_VIEW_REQUEST_CODE);
		}
	}

	private void chooseCert() {
		KeyChain.choosePrivateKeyAlias(getActivity(), this, new String[] { "RSA" }, null, null, -1, null);
	}

	private void installServerCertificate(String path) throws IOException {

		Intent intent = KeyChain.createInstallIntent();
		Certificate ca = null;
		InputStream caInput = null;
		File file = new File(path);
		if (file.exists()) {

			if (!ApplicationCacheHelper.copyFileToCache(file)) {
				return;
			} else {
				file = new File(ApplicationCacheHelper.getCachePath(true) + file.getName());
				prefManager.getSharedPreferences().edit().putString(SERVER_CERTIFICATE_PATH, file.getPath()).commit();
			}

			CertificateFactory cf;
			try {
				cf = CertificateFactory.getInstance("X.509");
				caInput = new BufferedInputStream(new FileInputStream(file));
				ca = cf.generateCertificate(caInput);
				System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
			} catch (CertificateException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				try {
					caInput.close();
					intent.putExtra(KeyChain.EXTRA_CERTIFICATE, ca.getEncoded());
					intent.putExtra(KeyChain.EXTRA_NAME, file.getName());
					startActivityForResult(intent, INSTALL_SERVER_CERT_INSTALL_DIALOG_REQUEST_CODE);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (CertificateEncodingException e) {
					e.printStackTrace();
				} finally {
					CheckBoxPreference pref = (CheckBoxPreference) findPreference(SERVER_CERT_REQUIRED);
					pref.setChecked(true);
				}
			}
		}
	}

	private boolean deleteCertificate(final String prefName, int questionRes, final int resultRes) {

		final String path = prefManager.getSharedPreferences().getString(prefName, null);

		if (StringUtils.hasText(path)) {
			new AlertDialog.Builder(getActivity()).setTitle(String.format(getString(R.string.pref_category_security))).setIcon(R.drawable.ic_launcher).setMessage(String.format(getString(questionRes), StringUtils.getFilename(path)))
					.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							try {
								if (ApplicationCacheHelper.removeFile(path)) {
									Toast.makeText(getActivity(), getString(resultRes), Toast.LENGTH_SHORT).show();
									prefManager.getSharedPreferences().edit().putString(prefName, null).commit();
									refreshServerCertificateState();

								}
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
						}
					}).show();
		}
		return true;
	}

	private void refreshServerCertificateState() {
		final String path = prefManager.getSharedPreferences().getString(SERVER_CERTIFICATE_PATH, null);
		Preference pref = findPreference(DELETE_SERVER_CERT_BUTTON);
		pref.setEnabled(StringUtils.hasText(path));
		pref = findPreference(SHOW_SERVER_CERT_BUTTON);
		pref.setEnabled(StringUtils.hasText(path));
		pref = findPreference(SERVER_CERT_REQUIRED);
		pref.setEnabled(StringUtils.hasText(path));
	}

	private void refreshClientCertificateState() {
		final String path = prefManager.getSharedPreferences().getString(CLIENT_CERTIFICATE_ALIAS, null);
		Preference pref = findPreference(DELETE_CLIENT_CERT_BUTTON);
		pref.setEnabled(StringUtils.hasText(path));
		pref = findPreference(SHOW_CLIENT_CERT_BUTTON);
		pref.setEnabled(StringUtils.hasText(path));
	}

	private class InstallClientCertificate extends AsyncTask<String, Void, Boolean> {

		private Exception error;

		@Override
		protected Boolean doInBackground(String... aliasParam) {
			try {
				String alias = aliasParam[0];
				if (!StringUtils.hasText(alias)) {
					return null;
				}

				PrivateKey pk = KeyChain.getPrivateKey(getActivity(), alias);
				X509Certificate[] chain = KeyChain.getCertificateChain(getActivity(), alias);

				byte[] data = "foobar".getBytes("ASCII");
				Signature sig = Signature.getInstance("SHA1withRSA");
				sig.initSign(pk);
				sig.update(data);
				byte[] signed = sig.sign();

				PublicKey pubk = chain[0].getPublicKey();
				sig.initVerify(pubk);
				sig.update(data);
				boolean valid = sig.verify(signed);
				Log.d("InstallClientCertificate", "signature is valid: " + valid);

				return valid;
			} catch (Exception e) {
				e.printStackTrace();
				error = e;
				return null;
			}
		}

		@Override
		protected void onPostExecute(Boolean valid) {
			if (error != null) {
				Toast.makeText(getActivity(), "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
				return;
			}
			if (valid) {
				Toast.makeText(getActivity(), getString(R.string.pref_show_client_cert_button_install_ok), Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getActivity(), getString(R.string.pref_show_client_cert_button_install_bad), Toast.LENGTH_SHORT).show();
			}
		}
	}
}
