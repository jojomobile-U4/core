package com.u4.mobile.base.ui;

public class NavigationDrawerListItem {
	private Class<?> activityClass;
	private Long count = 0L;
	private int icon = -1;
	private boolean isCounterVisible = false;
	private int title;

	public NavigationDrawerListItem() {
	}
	
	public NavigationDrawerListItem(int title, int icon, Class<?> activityClass) {
		this.title = title;
		this.icon = icon;
		this.activityClass = activityClass;
	}

	public NavigationDrawerListItem(int title, int icon, Class<?> activityClass, Long count) {
		this.title = title;
		this.icon = icon;
		this.activityClass = activityClass;
		this.isCounterVisible = true;
		this.count = count;
	}

	public Class<?> getActivityClass() {
		return activityClass;
	}

	public Long getCount() {
		return this.count;
	}

	public boolean getCounterVisibility() {
		return this.isCounterVisible;
	}

	public int getIcon() {
		return this.icon;
	}

	public int getTitle() {
		return this.title;
	}

	public void setActivityClass(Class<?> activityClass) {
		this.activityClass = activityClass;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public void setCounterVisibility(boolean isCounterVisible) {
		this.isCounterVisible = isCounterVisible;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public void setTitle(int title) {
		this.title = title;
	}
}
