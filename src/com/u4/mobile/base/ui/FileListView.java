package com.u4.mobile.base.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.*;

import com.u4.mobile.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

@EActivity(resName="file_list_view")
public class FileListView extends ListActivity {

	private List<String> item = null;
	private List<String> path = null;
	private String root;
	private TextView myPath;
	private static Intent resultIntent = new Intent();

	@Override
	protected void onCreate(android.os.Bundle savedInstanceState) {
		setTheme(R.style.PreferencesTheme);
		super.onCreate(savedInstanceState);
	};

	@AfterViews
	void startUp() {

		myPath = (TextView) findViewById(R.id.path);
		root = Environment.getExternalStorageDirectory().getPath();
		getDir(root);
		setResult(Activity.RESULT_CANCELED, resultIntent);
	}

	private void getDir(String dirPath) {
		myPath.setText("Location: " + dirPath);
		item = new ArrayList<String>();
		path = new ArrayList<String>();
		File f = new File(dirPath);
		File[] files = f.listFiles();

		if (files == null) {
			return;
		}
		if (!dirPath.equals(root)) {
			item.add(root);
			path.add(root);
			item.add(".." + File.separator);
			path.add(f.getParent());
		}

		for (int i = 0; i < files.length; i++) {
			File file = files[i];

			if (!file.isHidden() && file.canRead()) {
				path.add(file.getPath());
				if (file.isDirectory()) {
					item.add(file.getName() + File.separator);
				} else {
					item.add(file.getName());
				}
			}
		}

		FileListAdapter fileList = new FileListAdapter(this, item);
		setListAdapter(fileList);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		final File file = new File(path.get(position));

		if (file.isDirectory()) {
			if (file.canRead()) {
				getDir(path.get(position));
			} else {
				new AlertDialog.Builder(this).setIcon(R.drawable.ic_launcher).setTitle(String.format(getString(R.string.pref_category_security))).setMessage(String.format(getString(R.string.pref_category_security_install_err_msg), file.getName())).setPositiveButton("OK", null)
						.show();
			}
		} else {
			new AlertDialog.Builder(this).setIcon(R.drawable.ic_launcher).setTitle(String.format(getString(R.string.pref_category_security))).setMessage(String.format(getString(R.string.pref_category_security_install_question), file.getName()))
					.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							resultIntent.putExtra(AppSettingsFragment.INSTALL_SERVER_CERT_PATH, file.getPath());
							setResult(Activity.RESULT_OK, resultIntent);
							finish();
						}
					}).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
							resultIntent.removeExtra(AppSettingsFragment.INSTALL_SERVER_CERT_PATH);
							setResult(Activity.RESULT_CANCELED, resultIntent);
						}
					}).show();
		}
	}
}