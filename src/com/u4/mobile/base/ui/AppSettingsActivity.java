package com.u4.mobile.base.ui;

import org.androidannotations.annotations.*;

import com.u4.mobile.R;

import android.content.Intent;
import android.os.Bundle;

@EActivity
public class AppSettingsActivity extends ActivityBase {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.PreferencesTheme);
		super.onCreate(savedInstanceState);

		getFragmentManager().beginTransaction().replace(android.R.id.content, new AppSettingsFragment()).commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	
}
