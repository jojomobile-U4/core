package com.u4.mobile.base.ui;

import com.u4.mobile.base.app.ApplicationEnums.Fonts;
import com.u4.mobile.utils.FontHelper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditText_Roboto extends EditText {

	public EditText_Roboto(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		if (isInEditMode()) {
			return;
		}
		setTypeface(FontHelper.ParseFontAttributes(attrs));
	}

	public EditText_Roboto(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (isInEditMode()) {
			return;
		}

		setTypeface(FontHelper.ParseFontAttributes(attrs));
	}

	public EditText_Roboto(Context context) {
		super(context);

		if (isInEditMode()) {
			return;
		}
		setTypeface(FontHelper.ParseFontAttributes(context, Fonts.Roboto_Bold));
	}

	@Override
	public void setTypeface(Typeface tf) {
		super.setTypeface(tf);
	}

}
