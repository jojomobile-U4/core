package com.u4.mobile.base.ui;

import org.springframework.util.StringUtils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;

import com.u4.mobile.R;
import com.u4.mobile.base.app.ApplicationEnums;

import org.androidannotations.annotations.*;

@EActivity
public class LoginActivity extends ActivityBase {

	@Override
	@UiThread
	public void showDialogInfo(String message) {

		if (!StringUtils.hasText(message)) {
			message = ApplicationEnums.MessageEquivalent.NoMessage.getValue();
		}

		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(getResources().getString(R.string.app_name));
		alertDialog.setMessage(message);
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", (DialogInterface.OnClickListener) null);
		alertDialog.show();
	}

	@Override
	@UiThread
	protected void setLoaderVisibile(boolean visible) {
		if (loader != null) {
			loader.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}

}
