package com.u4.mobile.base.ui;

import java.util.List;

import com.u4.mobile.base.app.ApplicationInterfaces.IMainMenuFrafments;
import com.u4.mobile.base.ui.FragmentBase;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MainMenuSectionsPagerAdapter extends FragmentPagerAdapter {

	private final List<FragmentBase> fragments;

	public MainMenuSectionsPagerAdapter(FragmentManager fragmentManager, List<FragmentBase> fragments) {
		super(fragmentManager);
		this.fragments = fragments;
	}

	@Override
	public Fragment getItem(int position) {
		return this.fragments.get(position);
	}

	@Override
	public int getCount() {
		return this.fragments.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {

		Fragment fragment = getItem(position);

		if (fragment instanceof IMainMenuFrafments) {
			return ((IMainMenuFrafments) fragment).getPageTitle();
		}
		return null;
	}
}
