package com.u4.mobile.base.ui;

import java.io.File;

import com.u4.mobile.R;
import com.u4.mobile.base.app.MainApp;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ErrorDialogFragment extends DialogFragment {
	private String message;

	public ErrorDialogFragment() {
		super();
	}

	public void deleteFile() {
		final File crashFile = new File(MainApp.getInstance().getCrashFilePath());
		if (crashFile.exists()) {
			crashFile.delete();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		message = getArguments().getString(Intent.EXTRA_BUG_REPORT);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		getDialog().setTitle(R.string.alert_dialog__error__title);
		final View v = inflater.inflate(R.layout.error_dialog, container, false);

		final TextView view = (TextView) v.findViewById(R.id.error_dialog__message__text_view);
		String localizedMessage = message.split("\n")[0];
		if (localizedMessage == null || localizedMessage.trim().length() == 0) {
			localizedMessage = getString(R.string.unexpected_error_occured);
		}
		view.setText(getString(R.string.last_run_ended_abnormally, localizedMessage));

		final Button yesButton = (Button) v.findViewById(R.id.error_dialog__yes__button);
		yesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				sendReport();
				deleteFile();
				dismiss();
			}
		});

		final Button noButton = (Button) v.findViewById(R.id.error_dialog__no__text_view);
		noButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteFile();
				dismiss();
			}
		});
		return v;
	}

	public void sendReport() {
		final Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("plain/text");// setType("message/rfc822");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "k.musial@teta.com.pl" });
		intent.putExtra(Intent.EXTRA_SUBJECT, "[Teta Mobile SFA] Bug report");
		intent.putExtra(Intent.EXTRA_TEXT, message);
		startActivity(Intent.createChooser(intent, getString(R.string.error_dialog__header__text_view)));
	}
}