package com.u4.mobile.base.ui;

import java.util.ArrayList;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;

import android.content.Context;

@EBean(scope = Scope.Singleton)
public class NavigationDrawerHolder {

	private ArrayList<NavigationDrawerListItem> navigationDrawerItems = new ArrayList<NavigationDrawerListItem>();

	@RootContext
	Context context;

	public void addNavigationDrawerItem(NavigationDrawerListItem navigationDrawerListItem) {
		navigationDrawerItems.add(navigationDrawerListItem);
	}

	public void clear() {
		navigationDrawerItems.clear();
	}

	public ArrayList<NavigationDrawerListItem> getNavigationDrawerItems() {
		return navigationDrawerItems;
	}

}
