package com.u4.mobile.base.ui;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.u4.mobile.R;
import com.u4.mobile.base.bus.BusProvider;
import com.u4.mobile.scanner.EnterBarcodeDialog;
import com.u4.mobile.scanner.EnterBarcodeDialog.OnEnterBarcodeListener;
import com.u4.mobile.scanner.EnterBarcodeDialog_;
import com.u4.mobile.scanner.Scanner.Scanable;

@EFragment
public class FragmentBase extends Fragment {

	@Bean
	protected BusProvider mDataBus;

	@ViewById(resName = "loader__container")
	public RelativeLayout loader;

	@ViewById(resName = "loader__message")
	public TextView loaderMessage;

	private EditText currentControl;

	private boolean loading = true;

	@UiThread
	protected void showDialogInfo(int resId) {
		showDialogInfo(getString(resId));
	}

	@UiThread
	protected void showDialogInfo(String message) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setMessage(message).setTitle(R.string.alert_dialog__information__title)
				.setNeutralButton(R.string.ok, null);
		final DialogFragment df = new DialogFragment() {

			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {

				return builder.create();
			}
		};

		df.show(getFragmentManager(), "showInfo");
	}

	protected void showError(int resId) {
		showError(getString(resId));
	}

	@UiThread
	protected void showError(String message) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setMessage(message).setTitle(R.string.error_dialog_title).setNeutralButton(R.string.ok, null);
		final DialogFragment df = new DialogFragment() {

			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {

				return builder.create();
			}
		};

		df.show(getFragmentManager(), "errorDialog");
	}

	protected void showToast(int resId, int length) {
		showToast(getString(resId), length);
	}

	@UiThread
	protected void showToast(String msg, int length) {
		Toast.makeText(getActivity(), msg, length).show();
	}

	public EditText getCurrentControl() {
		View result = getActivity().getCurrentFocus();
		
		if (EditText.class.isInstance(result)) {
			currentControl = (EditText) result;
		}
		
		return currentControl;
	}

	public boolean isLoading() {
		return loading;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		if (Scanable.class.isInstance(this)) {
			inflater.inflate(R.menu.fragment_base_menu, menu);
		}

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.fragment_base_menu__enter_barcode) {
			final Scanable scanner = ((Scanable) this);
			final EnterBarcodeDialog dialog = EnterBarcodeDialog_.builder().build();
			dialog.setOnEnterBarcodeListener(new OnEnterBarcodeListener() {

				@Override
				public void onEnterBarcode(String data) {
					scanner.onBarcodeScan(data);
				}
			});

			dialog.show(getFragmentManager(), "EnterBarcodeDialog");

			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		
		mDataBus.unregister(this);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		if (Scanable.class.isInstance(this) && menu.findItem(R.id.fragment_base_menu__enter_barcode) == null) {
			menu.add(0, R.id.fragment_base_menu__enter_barcode, Menu.NONE, getString(R.string.enter_barcode_menu__title));
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mDataBus.register(this);
	}

	@UiThread
	public void setLoaderVisible(boolean visible) {
		setLoaderVisible(visible, -1);
	}

	@UiThread
	public void setLoaderVisible(boolean visible, int resId) {
		this.loading = visible;

		if (loader != null) {
			loader.setVisibility(visible ? View.VISIBLE : View.GONE);

			if (loaderMessage != null) {
				loaderMessage.setVisibility(visible && resId > 0 ? View.VISIBLE : View.GONE);

				if (visible && resId > 0) {
					loaderMessage.setText(resId);
				}
			}
		}

		if (getActivity() != null && !getActivity().isFinishing()) {
			getActivity().invalidateOptionsMenu();
		}
	}

	@AfterViews
	public void setupLoader() {
		if (Scanable.class.isInstance(this)) {
			setHasOptionsMenu(true);
		}

		loading = loader != null;
		if (getActivity() != null && !getActivity().isFinishing()) {
			getActivity().invalidateOptionsMenu();
		}
	}
}
