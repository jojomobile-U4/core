package com.u4.mobile.base.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.springframework.util.StringUtils;

import android.os.AsyncTask;
import android.os.Bundle;
import android.security.KeyChain;
import android.widget.TextView;

import org.androidannotations.annotations.*;
import org.androidannotations.annotations.sharedpreferences.*;

import com.u4.mobile.R;
import com.u4.mobile.base.app.AppSettings_;
import com.u4.mobile.utils.DateTimeHelper;
import com.u4.mobile.utils.LogHelper;
import com.u4.mobile.utils.DateTimeHelper.DateTimeFormat;

@EActivity(resName = "activity_security_certificate")
public class CertificateActivity extends ActivityBase {

	@ViewById(resName = "certificate_issued_to_common_name")
	TextView certificateIssuedToCommonName;

	@ViewById(resName = "certificate_issued_to_organization")
	TextView certificateIssuedToOrganization;

	@ViewById(resName = "certificate_issued_to_organizational_unit")
	TextView certificateIssuedToOrganizationalUnit;

	@ViewById(resName = "certificate_issued_to_serial_number")
	TextView certificateIssuedToSerialNumber;

	@ViewById(resName = "certificate_issued_by_common_name")
	TextView certificateIssuedByCommonName;

	@ViewById(resName = "certificate_issued_by_organization")
	TextView certificateIssuedByOrganization;

	@ViewById(resName = "certificate_issued_by_organizational_unit")
	TextView certificateIssuedByOrganizationalUnit;

	@ViewById(resName = "certificate_validity_issued_on")
	TextView certificateValidityIssuedOn;

	@ViewById(resName = "certificate_validity_expires_on")
	TextView certificateValidityExpiresOn;

	@ViewById(resName = "certificate_fingerprints_sha_256")
	TextView certificateFingerprintsSha256;

	@ViewById(resName = "certificate_fingerprints_sga_1")
	TextView certificateFingerprintsSga1;

	@Pref
	AppSettings_ appSettings;

	@Extra(AppSettingsFragment.CLIENT_CERTIFICATE_ALIAS)
	boolean isClientCertificate;

	private X509Certificate serverCertificate;
	private X509Certificate clientCertificate;

	@AfterViews
	void startUp() {

		if (!isClientCertificate) {
			getServerCertificate();

			if (serverCertificate != null) {
				fillField(serverCertificate);
			}
			return;
		}

		if (StringUtils.hasText(appSettings.clientCertificateAlias().get())) {
			new getClientCertificate().execute(appSettings.clientCertificateAlias().get());
		}
	}

	@Override
	protected void onCreate(android.os.Bundle savedInstanceState) {
		setTheme(R.style.PreferencesTheme);
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
			final Bundle extras = getIntent().getExtras();
			if (extras != null && extras.containsKey(AppSettingsFragment.CLIENT_CERTIFICATE_ALIAS)) {
				isClientCertificate = extras.getBoolean(AppSettingsFragment.CLIENT_CERTIFICATE_ALIAS);
			}
		}
	}

	private void getServerCertificate() {

		CertificateFactory cf = null;

		try {
			cf = CertificateFactory.getInstance("X509");

			File file = new File(appSettings.serverCertificatePath().get());
			if (file.exists()) {
				serverCertificate = (X509Certificate) cf.generateCertificate(new FileInputStream(file));
			}
		} catch (CertificateException e) {
			LogHelper.setErrorMessage("CertificateActivity.getServerCertificate", e);
		} catch (FileNotFoundException e) {
			LogHelper.setErrorMessage("CertificateActivity.getServerCertificate", e);
		}

		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			byte[] publicKey = md.digest(serverCertificate.getPublicKey().getEncoded());

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < publicKey.length; i++) {
				String appendString = Integer.toHexString(0xFF & publicKey[i]);
				if (appendString.length() == 1)
					hexString.append("0");
				hexString.append(appendString);
			}
		} catch (NoSuchAlgorithmException e1) {
			LogHelper.setErrorMessage("CertificateActivity.getServerCertificate", e1);
		}
	}

	private void fillField(X509Certificate cert) {

		if (cert == null) {
			return;
		}

		certificateIssuedToCommonName.setText(cert.getSubjectDN().getName());

		// TextView certificateIssuedToOrganization;

		// TextView certificateIssuedToOrganizationalUnit;

		// TextView certificateIssuedToSerialNumber;

		certificateIssuedByCommonName.setText(cert.getIssuerDN().getName());

		// TextView certificateIssuedByOrganization;

		// TextView certificateIssuedByOrganizationalUnit;

		certificateValidityIssuedOn.setText(DateTimeHelper.getDateAsString(cert.getNotBefore(), DateTimeFormat.OracleDateFormat));

		certificateValidityExpiresOn.setText(DateTimeHelper.getDateAsString(cert.getNotAfter(), DateTimeFormat.OracleDateFormat));

		// TextView certificateFingerprintsSha256;

		// TextView certificateFingerprintsSga1;

	}

	private class getClientCertificate extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... aliasParam) {
			try {
				String alias = aliasParam[0];
				if (!StringUtils.hasText(alias)) {
					return null;
				}

				X509Certificate[] chain = KeyChain.getCertificateChain(getBaseContext(), alias);

				for (int i = 0; i <= chain.length - 1; i++) {

					if (chain[i].getSubjectDN().getName().equalsIgnoreCase("CN=" + alias)) {
						clientCertificate = chain[i];
						return true;
					}
				}

				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(Boolean valid) {
			fillField(clientCertificate);
		}
	}


}
