package com.u4.mobile.base.ui;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.R;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.utils.ControlsHelper;
import com.u4.mobile.utils.LogHelper;

@EBean
public abstract class ListAdapterBase<T extends DownloadTransferObjectBase> extends BaseAdapter {

	public static final long SEARCH_DELAY_TIME = 500;

	private FragmentBase fragmentBase;
	private Long listOffset = 0L;
	private Long pageSize = 10L;
	private ArrayList<T> orginalCollection = new ArrayList<T>();
	private boolean reachedLastPage;
	private boolean hasMoreItems = true;
	private boolean withClear = false;
	private List<T> checkedItems = new ArrayList<T>();;
	private String highlightText;
	private String searchText;
	private boolean selecting = false;
	private long lastTextChanged;
	private Handler handler;
	private EditText searchView;
	private boolean inProgress = false;

	@RootContext
	public Context context;

	final private TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void afterTextChanged(Editable s) {

			lastTextChanged = System.currentTimeMillis();
			handler.postDelayed(new SearchDelayed(lastTextChanged), SEARCH_DELAY_TIME);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {

		}
	};

	public ListAdapterBase() {
		super();
	}

	public boolean add(T item) {

		return orginalCollection.add(item);
	}

	public void clear() {

		orginalCollection.clear();
	}

	public List<T> getCheckedItems() {

		return checkedItems;
	}

	public List<String> getCheckedItemsGuids() {
		List<String> guids = new ArrayList<String>();

		for (T item : checkedItems) {
			guids.add(item.getGuid());
		}

		return guids;
	}

	public List<Long> getCheckedItemsIds() {
		List<Long> ids = new ArrayList<Long>();

		for (T item : checkedItems) {
			ids.add(item.getId());
		}

		return ids;
	}

	public Context getContext() {
		return context;
	}

	@Override
	public int getCount() {
		return orginalCollection.size();
	}

	@Override
	public T getItem(int position) {
		return orginalCollection.get(position);
	}

	@Override
	public long getItemId(int position) {

		for (int i = 0; i <= orginalCollection.size() - 1; i++) {
			if (i == position) {
				return ((DownloadTransferObjectBase) orginalCollection.get(i)).getId();
			}
		}

		return -1;
	}

	public int getItemPosition(long id) {
		if (id < 0) {
			return 0;
		}

		for (int position = 0; position < orginalCollection.size(); position++) {
			if (getItem(position).getId() == id) {
				return position;
			}
		}

		return 0;
	}

	public void getMoreData() {
		reloadData(false, listOffset);
	}

	public Long getPageSize() {
		return pageSize;
	}

	public TextWatcher getTextWatcher() {
		return textWatcher;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (position == getCount() - 1 && hasMoreItems()) {
			reloadData(withClear, listOffset);
		}
		return null;
	}

	public void init(FragmentBase fragmentBase, Long pageSize) {
		this.fragmentBase = fragmentBase;
		this.setPageSize(pageSize);

		searchView = (EditText) fragmentBase.getView().findViewById(R.id.searchView);
		if (searchView != null) {
			setSearchText(searchView.getText().toString());
		}
		handler = new Handler();
	}

	public boolean isInProgress() {
		return inProgress;
	}

	public boolean isItemChecked(int position) {
		return checkedItems.contains(getItem(position));
	}

	public boolean isSelecting() {
		return selecting;
	}

	public abstract QueryBuilder<T, Long> prepareQuery();

	public void reloadData(boolean withClear, Long listOffset) {
		this.withClear = withClear;
		this.setListOffset(listOffset);

		if (searchView != null) {
			setSearchText(searchView.getText().toString());
		}

		loadDataInBackground();
	}

	public void setAllItemsChecked(boolean checked) {
		if (checked) {
			checkedItems.clear();
			checkedItems.addAll(orginalCollection);
		} else {
			checkedItems.clear();
		}

		notifyDataSetChanged();
	}

	public void setInProgress(boolean inProgress) {
		this.inProgress = inProgress;
	}

	public void setItemChecked(int position, boolean checked) {
		if (checked) {
			checkedItems.add(getItem(position));
		} else {
			checkedItems.remove(getItem(position));
		}

		notifyDataSetChanged();
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public void setSelecting(boolean selecting) {
		this.selecting = selecting;

		if (selecting) {
			checkedItems.clear();
		}
	}

	@UiThread
	public void updateUI(List<T> items) {
		if (withClear) {
			clear();
			withClear = false;
		}

		if (items != null) {
			for (final T w : items) {
				add(w);
				setHighlightText(searchText);
			}

			notifyDataSetChanged();
			notifyMoreItems(!reachedLastPage);
		}

		setInProgress(false);
		fragmentBase.setLoaderVisible(false);
	}

	protected List<T> getData() throws SQLException {
		return prepareQuery().query();
	}

	protected String getHighlightText() {
		return highlightText;
	}

	protected Long getListOffset() {
		return listOffset;
	}

	protected String getSearchText() {
		return searchText;
	}

	protected boolean hasMoreItems() {
		return hasMoreItems;
	}

	protected void highlightText(TextView textView, String text) {
		textView.setText(text);
		ControlsHelper.highlightText(textView, highlightText);
	}

	@Background
	protected void loadDataInBackground() {
		setInProgress(true);
		List<T> resultDocuments = null;

		if (hasMoreItems()) {
			try {
				resultDocuments = getData();

				if (resultDocuments.size() < getPageSize()) {
					reachedLastPage = true;
				} else {
					listOffset += pageSize;
				}
			} catch (final Exception e) {
				LogHelper.setErrorMessage(ListAdapterBase.class.getName(), e);
			}
		}

		updateUI(resultDocuments);
	}

	protected void notifyMoreItems(boolean more) {
		hasMoreItems = more;
	}

	protected void setHasMoreItems(boolean hasMoreItems) {
		this.hasMoreItems = hasMoreItems;
	}

	protected void setHighlightText(String highlightText) {
		this.highlightText = highlightText;
	}

	protected void setListOffset(Long listOffset) {
		this.listOffset = listOffset;

		if (listOffset == 0) {
			hasMoreItems = true;
		}
	}

	protected void setSearchText(String text) {
		this.searchText = text;
	}

	public FragmentBase getFragmentBase() {
		return fragmentBase;
	}

	class SearchDelayed implements Runnable {

		private final long time;

		public SearchDelayed(long time) {

			this.time = time;
		}

		@Override
		public void run() {

			if (time == lastTextChanged) {
				withClear = true;
				listOffset = 0L;
				hasMoreItems = true;
				reachedLastPage = false;
				reloadData(withClear, listOffset);
			}
		}
	}
}
