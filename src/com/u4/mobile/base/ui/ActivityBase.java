package com.u4.mobile.base.ui;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import com.u4.mobile.R;
import com.u4.mobile.base.bus.BusProvider;
import com.u4.mobile.scanner.Scanner;
import com.u4.mobile.scanner.Scanner.Scanable;
import com.u4.mobile.utils.LogHelper;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.RelativeLayout;

@EActivity
public class ActivityBase extends FragmentActivity {

	public static final int NOTIFY_BACK = -1;
	public static final int NOTIFY_OK = 0;
	public static final int NOTIFY_DISCARD_CHANGES = -2;

	private static IntentFilter receiverFilter = new IntentFilter("com.u4.mobile.motorola.RECVR");

	@Bean
	protected BusProvider mDataBus;

	@ViewById(resName = "loader__container")
	protected RelativeLayout loader;

	private final BroadcastReceiver scannerReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			for (int x = 0; x <= getSupportFragmentManager().getFragments().size() - 1; x++) {
				FragmentBase frag = (FragmentBase) getSupportFragmentManager().getFragments().get(x);

				if (Scanable.class.isInstance(frag) && frag.getUserVisibleHint()) {

					((Scanable) frag).onBarcodeScan(Scanner.handleDecodeData(intent));
				}
			}
		}
	};

	public FragmentBase getActiveFragment() {
		if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
			return null;
		}
		
		String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
		
		return (FragmentBase) getSupportFragmentManager().findFragmentByTag(tag);
	}

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		mDataBus.unregister(this);
		
		if (getActionBar() != null) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public void onNewIntent(Intent i) {
		super.onNewIntent(i);
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		try {
			unregisterReceiver(scannerReceiver);
		} catch (Exception e) {
			LogHelper.setErrorMessage("ActivityBase.unregisterReceiver(scannerReceiver)", e);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		mDataBus.register(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		registerReceiver(scannerReceiver, receiverFilter);
	}

	@UiThread
	protected void setLoaderVisibile(boolean visible) {
		if (loader != null) {
			loader.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}

	@UiThread
	public void showDialogInfo(String message) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(getResources().getString(R.string.app_name));
		alertDialog.setMessage(message);
		alertDialog.setIcon(R.drawable.app_logo);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", (DialogInterface.OnClickListener) null);
		alertDialog.show();
	}

}
