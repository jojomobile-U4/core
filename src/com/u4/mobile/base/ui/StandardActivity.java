package com.u4.mobile.base.ui;

import org.androidannotations.annotations.EActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.u4.mobile.R;

@EActivity
public abstract class StandardActivity extends ActivityBase {

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_standard);

		registerFragments();
	}

	public abstract void registerFragments();

	public void addFragment(Fragment fragment) {

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.activity_standard__content, fragment).commit();
	}

}
