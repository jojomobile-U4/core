package com.u4.mobile.base.ui;

import java.util.ArrayList;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.u4.mobile.utils.LogHelper;

@EBean
public class NavigationDrawerListAdapter extends BaseAdapter {

	private ArrayList<NavigationDrawerListItem> items = new ArrayList<NavigationDrawerListItem>();

	@Bean
	NavigationDrawerHolder navigationDrawerHelper;

	@RootContext
	public Context context;

	public NavigationDrawerListAdapter() {
		super();
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public NavigationDrawerListItem getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public int getPosition(Class<?> activityClass) {
		int position = -1;

		for (int i = 0; i < items.size(); i++) {
			if (items.get(i).getActivityClass().equals(activityClass)) {
				position = i;
				break;
			}
		}

		return position;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		NavigationDrawerListItemView itemView = null;

		try {
			if (convertView == null) {
				itemView = NavigationDrawerListItemView_.build(context);
			} else {
				itemView = (NavigationDrawerListItemView) convertView;
			}

			itemView.populateView(getItem(position));

			return itemView;
		} catch (final Exception e) {
			LogHelper.setErrorMessage(NavigationDrawerListAdapter.class.getName(), e);
		}

		return convertView;
	}

	public void loadData() {
		loadDataInBackground();
	}

	public void setItems(ArrayList<NavigationDrawerListItem> items) {
		this.items = items;
	}

	@UiThread
	public void updateUI() {
		notifyDataSetChanged();
	}

	@Background
	protected void loadDataInBackground() {

		items.clear();
		items = navigationDrawerHelper.getNavigationDrawerItems();

		updateUI();
	}
}
