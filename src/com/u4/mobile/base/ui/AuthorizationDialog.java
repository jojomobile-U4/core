package com.u4.mobile.base.ui;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.springframework.util.StringUtils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.u4.mobile.R;
import com.u4.mobile.base.app.AppSettings_;
import com.u4.mobile.base.helpers.NetworkHelper;
import com.u4.mobile.model.SessionInfo;

@EFragment(resName = "authorization_dialog")
public abstract class AuthorizationDialog extends DialogFragmentBase {

	public static final String TITLE = "title";
	private OnAuthorizeListener listener;
	private EditText currentControl;

	@Pref
	public AppSettings_ appSettings;

	@FragmentArg(TITLE)
	public String title;

	@ViewById(resName = "authorization_dialog__user_name")
	public EditText userNameEditText;

	@ViewById(resName = "authorization_dialog__password")
	public EditText passwordEditText;

	@ViewById(resName = "loader__container")
	protected RelativeLayout loader;

	@Background
	public void authorize(String userName, String password) {
		try {
			SessionInfo sessionInfo = doAuthorize(userName, password);

			onAuthorized(sessionInfo);
		} catch (Exception exception) {
			showDialogInfo(NetworkHelper.translateMessage(exception, null));
		} finally {
			setLoaderVisibile(false);
		}
	}

	@Click(resName = "authorization_dialog__authorize_button")
	public void buttonAuthorizeClick() {
		if (validateControl(userNameEditText) && validateControl(passwordEditText)) {

			if (StringUtils.hasText(appSettings.userName().get()) && appSettings.userName().get().equalsIgnoreCase(userNameEditText.getText().toString())) {
				showDialogInfo(getString(R.string.authorization_dialog__msg_current_user_already_authorized));

				return;
			}

			setLoaderVisibile(true);
			authorize(userNameEditText.getText().toString(), passwordEditText.getText().toString());
		}
	}

	public abstract SessionInfo doAuthorize(String userName, String password);

	@UiThread
	public void onAuthorized(SessionInfo sessionInfo) {
		dismiss();

		if (sessionInfo != null && listener != null) {
			listener.onAuthorize(sessionInfo);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		getDialog().setTitle(R.string.authorization_dialog__title);
		final View view = inflater.inflate(R.layout.authorization_dialog, container, false);

		return view;
	}

	@AfterViews
	public void populateView() {
		setLoaderVisibile(false);

		if (StringUtils.hasText(title)) {
			getDialog().setTitle(title);
		}
	}

	public void setOnAuthorizeListener(OnAuthorizeListener listener) {
		this.listener = listener;
	}

	@UiThread
	protected void setLoaderVisibile(boolean visible) {
		if (loader != null) {
			loader.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}

	public static interface OnAuthorizeListener {
		public void onAuthorize(SessionInfo sessionInfo);
	}

	@Override
	public void onBarcodeScan(String data) {
		super.onBarcodeScan(data);

		if (getCurrentControl() != null) {
			getCurrentControl().setText(data);

			if (StringUtils.hasText(getCurrentControl().getText()) && getCurrentControl().getId() == R.id.authorization_dialog__password) {
				if (StringUtils.hasLength(userNameEditText.getText())) {
					View nextControl = getCurrentControl().focusSearch(View.FOCUS_UP);
					if (nextControl != null) {
						nextControl.requestFocus();
					}
					buttonAuthorizeClick();
					return;
				}
			}

			View nextControl = getCurrentControl().focusSearch(View.FOCUS_DOWN);
			if (nextControl != null) {
				nextControl.requestFocus();
			}
		}
	}

	public EditText getCurrentControl() {
		View result = getDialog().getCurrentFocus();
		if (EditText.class.isInstance(result)) {
			currentControl = (EditText) result;
		}
		return currentControl;
	}

}
