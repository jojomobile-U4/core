package com.u4.mobile.base.ui;

import java.text.NumberFormat;
import java.util.Locale;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.TextView;

import com.u4.mobile.R;
import com.u4.mobile.utils.CalculationHelper;

public class RequiredFieldValidator implements OnFocusChangeListener, TextWatcher {
	private TextView viewToValidate;
	private final boolean nonzero;
	private boolean validateMinMaxValue = false;
	private double minValue = Double.MIN_VALUE;
	private double maxValue = Double.MAX_VALUE;

	public RequiredFieldValidator(TextView viewToValidate, boolean nonzero) {
		this.viewToValidate = viewToValidate;
		this.nonzero = nonzero;
	}

	@Override
	public void afterTextChanged(Editable s) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	public boolean isValidateMinMaxValue() {
		return validateMinMaxValue;
	}

	public void notifyValidation() {
		onValidate(viewToValidate, false);
	}

	@Override
	public void onFocusChange(View view, boolean hasFocus) {
		if (!hasFocus) {
			onValidate(viewToValidate, false);
		}
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		onValidate(viewToValidate, false);
	}

	public void onValidate(TextView view, boolean validated) {
		if (!validated) {
			validate();
		} else {
			view.post(new Runnable() {
				@Override
				public void run() {
					viewToValidate.setError(null);
				}
			});
		}
	}

	public void setValidateMinMaxValue(boolean validate, double minValue, double maxValue) {
		validateMinMaxValue = validate;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	private void validate() {
		boolean number = false;
		
		if (nonzero) {
			try {
				NumberFormat.getNumberInstance(Locale.getDefault()).parse(viewToValidate.getText().toString()).doubleValue();
				number = true;
			} catch (final Exception e) {
			}
		}
		
		final String error;
		
		if (viewToValidate.getText() == null || viewToValidate.getText().toString().trim().length() == 0) {
			error = viewToValidate.getResources().getString(R.string.required_field);
		} else if (number && CalculationHelper.parseDouble(viewToValidate.getText().toString()) == 0.0d) {
			error = viewToValidate.getResources().getString(R.string.value_must_be_non_zero);
		} else if (isValidateMinMaxValue()
				&& !(CalculationHelper.parseDouble(viewToValidate.getText().toString()) >= minValue && CalculationHelper.parseDouble(viewToValidate.getText().toString()) <= maxValue)) {
			error = String.format(viewToValidate.getResources().getString(R.string.value_min_max_validation), minValue, maxValue);
		} else {
			error = null;
		}
		
		viewToValidate.setTag(R.id.edit_text_error, error);
		viewToValidate.post(new Runnable() {
			@Override
			public void run() {
				viewToValidate.setError(error);
			}
		});
	}
}