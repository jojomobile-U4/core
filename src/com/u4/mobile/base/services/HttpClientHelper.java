package com.u4.mobile.base.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.androidannotations.annotations.*;
import org.androidannotations.annotations.EBean.Scope;
import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

@EBean(scope = Scope.Singleton)
public class HttpClientHelper {

	private DefaultHttpClient httpClient;
	private String userName;
	private String userPassword;
	private String serverCertificatePath;
	private URL rootUrl;
	private URL proxyUrl;

	public DefaultHttpClient getHttpClient() {

		if (httpClient == null) {
			httpClient = getNewHttpClient();
		}
		return httpClient;
	}

	public void setupHttpClient(String user, String password, URL rootUrl, URL proxyUrl, String serverCertificatePath) {

		this.userName = user;
		this.userPassword = password;
		this.rootUrl = rootUrl;
		this.proxyUrl = proxyUrl;
		this.serverCertificatePath = serverCertificatePath;
	}

	public void updateCredentials(String user, String password) {

		this.userName = user;
		this.userPassword = password;

		if (httpClient != null) {
			getHttpClient().getCredentialsProvider().setCredentials(getAuthScope(), getCredentials());
		}
	}

	public void resetHttpClient(String user, String password, URL rootUrl, URL proxyUrl, String serverCertificatePath) {

		setupHttpClient(user, password, rootUrl, proxyUrl, serverCertificatePath);
		httpClient = null;
	}

	private Credentials getCredentials() {
		return userName != null ? new UsernamePasswordCredentials(userName, userPassword) : null;
	}

	
	private DefaultHttpClient getNewHttpClient() {
		try {
			if (rootUrl == null || userName == null || userPassword == null) {
				return null;
			}

			final HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
			HttpConnectionParams.setStaleCheckingEnabled(params, false);

			HttpConnectionParams.setConnectionTimeout(params, 30000);
			HttpConnectionParams.setSoTimeout(params, 30000);

			final SchemeRegistry registry = new SchemeRegistry();
			Certificate ca = null;

			if (rootUrl.getProtocol().equalsIgnoreCase(ProtocolType.HTTP.toString())) {
				if (rootUrl.getPort() > 0) {
					registry.register(new Scheme(ProtocolType.HTTP.toString(), PlainSocketFactory.getSocketFactory(), rootUrl.getPort()));
				} else {
					registry.register(new Scheme(ProtocolType.HTTP.toString(), PlainSocketFactory.getSocketFactory(), 80));
				}
			}
			if (rootUrl.getProtocol().equalsIgnoreCase(ProtocolType.HTTPS.toString())) {

				File file = new File(serverCertificatePath);
				if (file.exists()) {
					CertificateFactory cf = CertificateFactory.getInstance("X.509");
					InputStream caInput = new BufferedInputStream(new FileInputStream(file));
					try {
						ca = cf.generateCertificate(caInput);
						System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
					} finally {
						caInput.close();
					}
				}

				final KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
				keyStore.load(null, null);
				if (ca != null) {
					keyStore.setCertificateEntry("ca", ca);
				}

				// TODO ::false, ""
				final SSLSocketFactory sf = new ServiceSSLSocketFactory(keyStore, false, "");
				sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

				if (rootUrl.getPort() > 0) {

					registry.register(new Scheme(ProtocolType.HTTPS.toString(), sf, rootUrl.getPort()));
				} else {
					registry.register(new Scheme(ProtocolType.HTTPS.toString(), sf, 443));
				}
			}

			final ThreadSafeClientConnManager ccm = new ThreadSafeClientConnManager(params, registry);
			final DefaultHttpClient httpClient = new DefaultHttpClient(ccm, params);

			if (proxyUrl != null) {
				HttpHost proxy = new HttpHost(proxyUrl.getHost(), proxyUrl.getPort(), proxyUrl.getProtocol());
				httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
			}

			httpClient.getCredentialsProvider().setCredentials(getAuthScope(), getCredentials());

			if (httpClient.getCredentialsProvider().getCredentials(getAuthScope()) instanceof NTCredentials) {
				httpClient.getAuthSchemes().register("ntlm", new NTLMSchemeFactory());
			}
			return httpClient;
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private enum ProtocolType {
		HTTP, HTTPS
	};

	private static AuthScope getAuthScope() {
		return new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT);
	}

}