package com.u4.mobile.base.services;

import java.io.IOException;
import java.nio.charset.Charset;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

public class StreamHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

	public StreamHttpMessageConverter() {

		super(new MediaType("application", "octet-stream", DEFAULT_CHARSET));
	}

	@Override
	protected Object readInternal(Class<? extends Object> arg0, HttpInputMessage arg1) throws IOException, HttpMessageNotReadableException {
		return null;
	}

	@Override
	protected boolean supports(Class<?> arg0) {
		return false;
	}

	@Override
	protected void writeInternal(Object arg0, HttpOutputMessage arg1) throws IOException, HttpMessageNotWritableException {}

}