package com.u4.mobile.base.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

public class GsonHttpMessageConverter extends AbstractHttpMessageConverter<Object> {

	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
	private final Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new NetDateTimeAdapter()).registerTypeAdapter(UUID.class, new UUIDAdapter()).create();

	public GsonHttpMessageConverter() {

		super(new MediaType("application", "json", DEFAULT_CHARSET));
	}

	@Override
	protected Object readInternal(Class<? extends Object> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {

		try {
			return gson.fromJson(convertStreamToString(inputMessage.getBody()), clazz);
		} catch (final JsonSyntaxException e) {
			throw new HttpMessageNotReadableException("Could not read JSON: " + e.getMessage(), e);
		}

	}

	@Override
	protected boolean supports(Class<?> clazz) {

		return true;
	}

	@Override
	protected void writeInternal(Object t, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {

		// try {
		final String json = gson.toJson(t);

		outputMessage.getBody().write(json.getBytes());
		// } catch (Exception ex) {
		// String a = ex.getMessage();
		// }
	}

	public String convertStreamToString(InputStream is) throws IOException {

		if (is != null) {
			final Writer writer = new StringWriter();

			final char[] buffer = new char[1024];
			try {
				final Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {
			return "";
		}
	}

	public static class UUIDAdapter extends TypeAdapter<UUID> {

		@Override
		public UUID read(JsonReader reader) throws IOException {

			if (reader.peek() == JsonToken.NULL) {
				reader.nextNull();
				return null;
			}
			final String str = reader.nextString();
			if (!TextUtils.isEmpty(str)) {
				final Pattern p = Pattern.compile("([0-9a-f]{8})\\-?([0-9a-f]{4})\\-?([0-9a-f]{4})\\-?([0-9a-f]{4})\\-?([0-9a-f]{12})");
				final Matcher m = p.matcher(str);
				if (m.find()) {
					return UUID.fromString(m.group(1) + "-" + m.group(2) + "-" + m.group(3) + "-" + m.group(4) + "-" + m.group(5));
				}
			}
			return null;
		}

		@Override
		public void write(JsonWriter writer, UUID value) throws IOException {

			writer.value(value.toString());
		}
	}

	public static class NetDateTimeAdapter extends TypeAdapter<Date> {

		@Override
		public Date read(JsonReader reader) throws IOException {

			if (reader.peek() == JsonToken.NULL) {
				reader.nextNull();
				return null;
			}
			Date result = null;
			String str = reader.nextString();
			str = str.replaceAll("[^0-9]", "");
			if (!TextUtils.isEmpty(str)) {
				try {
					result = new Date(Long.parseLong(str));
				} catch (final NumberFormatException e) {
				}
			}
			return result;
		}

		@Override
		public void write(JsonWriter writer, Date value) throws IOException {

			// FIXME: current timezone
			String str = null;
			if (value != null) {
				str = "\\/Date(" + value.getTime() + "+0100)/";

			} else {
				str = null;
			}
			writer.value(str);
		}
	}

}