package com.u4.mobile.base.services;


import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import com.u4.mobile.model.DatabaseHolder;

@EBean(scope = Scope.Singleton)
public class Downloader {

	@Bean
	RestHelperBase restHelper;

	public void download() {

		DatabaseHolder.getInstance().recreateDb();
	}

	
	/*private <T> boolean save(Class<T> clazz, ResponseEntity<T[]> responseEntity) {

		List<T> result = null;

		if (responseEntity != null && responseEntity.hasBody() && responseEntity.getStatusCode() == HttpStatus.OK) {
			result = Arrays.asList(responseEntity.getBody());
		} else {
			return false;
		}

		final Dao<T, Long> dao = DaoManager.lookupDao(clazz);

		for (int i = 0; i <= result.size() - 1; i++) {
			try {
				dao.createOrUpdate(result.get(i));
			} catch (final SQLException e) {
				return false;
			}
		}
		return true;
	}*/
}
