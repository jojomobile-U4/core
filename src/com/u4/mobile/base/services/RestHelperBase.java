package com.u4.mobile.base.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.api.rest.RestClientRootUrl;
import org.androidannotations.api.rest.RestClientSupport;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.StringUtils;

import com.u4.mobile.utils.LogHelper;

@EBean(scope = Scope.Singleton)
public class RestHelperBase {

	private String userName;
	private String userPassword;
	private URL rootUrl;
	private URL proxyUrl;
	private Object restService;

	@Bean
	public HttpClientHelper httpClientHelper;

	void setupRestClient() {

		if (rootUrl != null) {
			((RestClientRootUrl) restService).setRootUrl(rootUrl.toString());
		}

		final HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setHttpClient(httpClientHelper.getHttpClient());
		((RestClientSupport) restService).getRestTemplate().setRequestFactory(requestFactory);
		final List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		interceptors.add(new ClientHttpRequestInterceptor() {

			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] arg1, ClientHttpRequestExecution execution) throws IOException {

				try {
					LogHelper.setDebugMessage("RestHelper", request.getURI().toString());
					return execution.execute(request, arg1);
				} catch (final Exception e) {
					LogHelper.setDebugMessage("RestHelper Exception", e.getMessage());
					throw new IOException(e);
				}
			}
		});
		((RestClientSupport) restService).getRestTemplate().setInterceptors(interceptors);
	}

	public void setupRestClient(String user, String password, String rootUrl, String proxyUrl, String serverCertificatePath) {

		this.userName = user;
		this.userPassword = password;
		this.rootUrl = getUrl(rootUrl, user.indexOf("\\") > 0);
		this.proxyUrl = getUrl(proxyUrl, user.indexOf("\\") > 0);

		httpClientHelper.setupHttpClient(this.userName, this.userPassword, this.rootUrl, this.proxyUrl, serverCertificatePath);

		setupRestClient();
	}

	public void resetRestClient(String user, String password, String rootUrl, String proxyUrl, String serverCertificatePath) {

		this.userName = user;
		this.userPassword = password;
		this.rootUrl = getUrl(rootUrl, user.indexOf("\\") > 0);
		this.proxyUrl = getUrl(proxyUrl, user.indexOf("\\") > 0);

		httpClientHelper.resetHttpClient(this.userName, this.userPassword, this.rootUrl, this.proxyUrl, serverCertificatePath);

		setupRestClient();
	}

	public void init(Object rest) {
		restService = rest;
	}

	private URL getUrl(String url, boolean isWinAuth) {

		if(!StringUtils.hasText(url)){
			return null;
		}
		
		try {
			
			if(isWinAuth){
				return new URL(url + (url.endsWith("/") ? "WinAuth/" : "/WinAuth/"));
			} else {
				
				return new URL(url + (url.endsWith("/") ? "BasicAuth/" : "/BasicAuth/"));
			}
		} catch (MalformedURLException e) {
			LogHelper.setErrorMessage(String.format("RestHelper.getRootUrl(URL rootUrl):rootUrl={0}", url), e);
		}
		return null;

	}

}
