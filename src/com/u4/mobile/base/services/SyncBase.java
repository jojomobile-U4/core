package com.u4.mobile.base.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.google.gson.Gson;
import com.u4.mobile.model.DatabaseHolder;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.utils.LogHelper;

public class SyncBase<T extends DownloadTransferObjectBase> {

	private final Class<T> structureClass;

	public SyncBase(Class<T> currentStructureObject) {

		structureClass = currentStructureObject;
	}

	public void save(String entityString) throws SQLException {

		final StructureBaseArrayTypeAdapter<T> structureBaseArrayTypeAdapter = new StructureBaseArrayTypeAdapter<T>(structureClass, new Gson());
		List<String> queries;

		try {
			queries = structureBaseArrayTypeAdapter.getQueries(entityString);
		} catch (final IOException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
			return;
		}
		if (queries != null) {
			final android.database.sqlite.SQLiteDatabase db = DatabaseHolder.getInstance().getWritableDatabase();
			db.beginTransaction();
			for (final String q : queries) {
				db.execSQL(q);
			}

			db.setTransactionSuccessful();
			db.endTransaction();
		}
	}
}
