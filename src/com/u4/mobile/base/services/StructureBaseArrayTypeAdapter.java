package com.u4.mobile.base.services;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import android.content.pm.ApplicationInfo;
import android.database.DatabaseUtils;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.field.DatabaseField;
import com.u4.mobile.base.app.MainApp;
import com.u4.mobile.model.DownloadTransferObjectBase;
import com.u4.mobile.utils.CalculationHelper;
import com.u4.mobile.utils.LogHelper;

public class StructureBaseArrayTypeAdapter<T> extends TypeAdapter<List<T>> {

	private final Class<T> clazz;
	private final Gson gson;
	private String maxSyncVersion;

	public StructureBaseArrayTypeAdapter(Class<T> clazz, Gson gson) {

		this.clazz = clazz;
		this.gson = gson;
	}

	private void addFieldsBySerializedNames(Class<? super T> clazz, Field[] fields, Map<String, Field> fieldsMap) {

		if (clazz.getSuperclass() != null) {
			addFieldsBySerializedNames(clazz.getSuperclass(), clazz.getSuperclass().getDeclaredFields(), fieldsMap);
		}
		SerializedName name = null;
		String fieldName = null;
		for (final Field field : fields) {
			field.setAccessible(true);
			name = field.getAnnotation(SerializedName.class);
			if (name != null) {
				fieldName = name.value().toUpperCase(Locale.getDefault());
				if (!fieldsMap.containsKey(fieldName) || field.getAnnotation(DatabaseField.class) != null) {
					fieldsMap.put(fieldName, field);
				}
			}
		}
	}

	@Override
	public List<T> read(JsonReader in) throws IOException {

		final boolean debuggable = 0 != (MainApp.getInstance().getApplicationContext().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE);
		final Map<String, Field> fieldsMap = new HashMap<String, Field>();
		addFieldsBySerializedNames(clazz, clazz.getDeclaredFields(), fieldsMap);
		// addFieldsBySerializedNames(clazz.getSuperclass().getDeclaredFields(),
		// fieldsMap, false);
		T obj;
		maxSyncVersion = null;
		final List<T> results = new ArrayList<T>();
		in.beginArray();
		while (in.hasNext()) {
			try {
				obj = clazz.newInstance();
			} catch (final InstantiationException e) {
				throw new IOException(e);
			} catch (final IllegalAccessException e) {
				throw new IOException(e);
			}
			in.beginObject();
			while (in.hasNext()) {
				final String name = in.nextName().toUpperCase(Locale.getDefault());
				final Field field = fieldsMap.get(name);
				if (field == null) {
					if (!"ID".equals(name) && debuggable) {
						LogHelper.setErrorMessage("read: field not found", clazz.getName() + "." + name);
					}
					in.skipValue();
					continue;
				}
				final JsonToken type = in.peek();
				Object value = null;
				switch (type) {
					case BEGIN_OBJECT:
						final TypeAdapter<?> adapter = gson.getAdapter(field.getType());
						value = adapter.read(in);
						break;
					case STRING:
						try {
							if (field.getType().isEnum()) {
								@SuppressWarnings({ "rawtypes", "unchecked" })
								final Class<? extends Enum> enumObject = (Class<? extends Enum>) Class.forName(field.getType().getName());
								@SuppressWarnings("unchecked")
								final Enum<?> en = Enum.valueOf(enumObject, in.nextString());
								value = en;
							} else {
								value = in.nextString();
								if (((String) value).startsWith("/Date")) {
									value = Long.parseLong(((String) value).substring(6, 19));
								}
							}
						} catch (final IllegalArgumentException e) {
							System.err.println(e.getMessage() + "#" + field.getName());
						} catch (final ClassNotFoundException e) {
							System.err.println(e.getMessage() + "#" + field.getName());
						} catch (final SecurityException e) {
							System.err.println(e.getMessage() + "#" + field.getName());
						}
						break;
					case NUMBER:
						if (field.getType().equals(long.class) || field.getType().equals(Long.class) || DownloadTransferObjectBase.class.isAssignableFrom(field.getType())) {
							value = in.nextLong();
						} else if (field.getType().equals(int.class) || field.getType().equals(Integer.class)) {
							value = in.nextInt();
						} else if (field.getType().equals(short.class) || field.getType().equals(Short.class)) {
							value = CalculationHelper.parseShort(in.nextString());
						} else if (field.getType().equals(float.class) || field.getType().equals(Float.class)) {
							value = Double.valueOf(in.nextDouble()).floatValue();
						} else if (field.getType().equals(String.class)) {
							value = in.nextString();
						} else {
							value = in.nextDouble();
						}

						break;
					case BOOLEAN:
						value = in.nextBoolean();
						break;
					case NULL:
						in.nextNull();
						value = null;
						break;
					default:
						break;
				}
				try {
					// TODO: Tu obsluga guidow
					// if
					// (DownloadGuidTransferObjectBase.class.isAssignableFrom(field.getType()))
					// {
					// DownloadGuidTransferObjectBase val;
					// val = (DownloadGuidTransferObjectBase)
					// field.getType().newInstance();
					// val.setGuid(UUID.fromString((String) value));
					// field.set(obj, val);
					// } else if
					// (DownloadTransferObjectBase.class.isAssignableFrom(field.getType()))
					// {
					if (DownloadTransferObjectBase.class.isAssignableFrom(field.getType())) {
						DownloadTransferObjectBase val;
						val = (DownloadTransferObjectBase) field.getType().newInstance();
						val.setId((Long) value);
						field.set(obj, val);
					} else if (field.getType().equals(String.class)) {
						field.set(obj, value.toString());
					} else if (field.getType().equals(UUID.class)) {
						field.set(obj, UUID.fromString((String) value));
					} else {
						field.set(obj, value);
					}
					if ("syncVersion".equals(field.getName())) {
						maxSyncVersion = (String) field.get(obj);
					}
				} catch (final IllegalArgumentException e) {
					System.err.println(field.getName() + " " + value);
					throw new IOException(e);
				} catch (final InstantiationException e) {
					throw new IOException(e);
				} catch (final IllegalAccessException e) {
					throw new IOException(e);
				}
			}
			in.endObject();
			results.add(obj);
		}
		in.endArray();
		return results;
	}

	@Override
	public void write(JsonWriter arg0, List<T> arg1) throws IOException {

		throw new IllegalStateException("Use StructureBaseTypeAdapter.write() instead.");
	}

	public List<String> getQueries(String entityString) throws IOException {

		final boolean debuggable = 0 != (MainApp.getInstance().getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE);
		final JsonReader in = new JsonReader(new StringReader(entityString));

		final Map<String, Field> fieldsMap = new HashMap<String, Field>();
		addFieldsBySerializedNames(clazz, clazz.getDeclaredFields(), fieldsMap);
		// addFieldsBySerializedNames(clazz.getSuperclass().getDeclaredFields(),
		// fieldsMap, true);

		DownloadTransferObjectBase strBaseObj = null;
		try {
			strBaseObj = (DownloadTransferObjectBase) clazz.newInstance();
		} catch (final InstantiationException e) {
			in.close();
			throw new IOException(e);
		} catch (final IllegalAccessException e) {
			in.close();
			throw new IOException(e);
		}

		final List<String> queries = new ArrayList<String>();
		StringBuilder queryBuilder = new StringBuilder("INSERT OR REPLACE INTO ");
		queryBuilder.append(strBaseObj.getMainTableName());
		queryBuilder.append(" (");
		int fi = 0;
		DatabaseField dbField;
		final List<String> dbFields = new ArrayList<String>();
		for (final Field f : fieldsMap.values()) {
			dbField = f.getAnnotation(DatabaseField.class);
			if (dbField == null) {
				continue;
			} else {
				dbFields.add(f.getName());
			}
			if (fi > 0) {
				queryBuilder.append(",");
			}
			DatabaseUtils.appendEscapedSQLString(queryBuilder, dbField.columnName());
			fi++;
		}
		queryBuilder.append(") VALUES ");
		final String insert = queryBuilder.toString();

		in.setLenient(true);
		in.beginArray();
		maxSyncVersion = null;
		while (in.hasNext()) {
			final Map<String, String> values = new HashMap<String, String>();
			in.beginObject();
			while (in.hasNext()) {
				final String name = in.nextName().toUpperCase(Locale.getDefault());
				final Field field = fieldsMap.get(name);
				if (field == null) {
					if (!"ID".equals(name) && debuggable) {
						LogHelper.setErrorMessage("save: field not found", clazz.getName() + "." + name);
					}
					in.skipValue();
					continue;
				}
				final JsonToken type = in.peek();
				String value = null;
				switch (type) {
					case BEGIN_OBJECT:
						final TypeAdapter<?> adapter = gson.getAdapter(field.getType());
						value = adapter.read(in).toString();
						break;
					case STRING:
						value = in.nextString();
						if (value.startsWith("/Date")) {
							value = value.substring(6, 19);
						}
						break;
					case NUMBER:
						if (field.getType().equals(long.class) || field.getType().equals(Long.class)) {
							value = Long.toString(in.nextLong());
						} else if (field.getType().equals(int.class) || field.getType().equals(Integer.class)) {
							value = Integer.toString(in.nextInt());
						} else if (field.getType().equals(short.class) || field.getType().equals(Short.class)) {
							value = Short.toString(CalculationHelper.parseShort(in.nextString()));
						} else if (field.getType().equals(float.class) || field.getType().equals(Float.class)) {
							value = Float.toString(Double.valueOf(in.nextDouble()).floatValue());
						} else if (field.getType().equals(String.class)) {
							value = in.nextString();
						} else {
							value = Double.toString(in.nextDouble());
						}

						break;
					case BOOLEAN:
						value = in.nextBoolean() ? "1" : "0";
						break;
					case NULL:
						in.nextNull();
						value = null;
						break;
					default:
						break;
				}
				values.put(field.getName(), value);
			}
			in.endObject();
			fi = 0;
			queryBuilder = new StringBuilder(insert);
			queryBuilder.append("(");
			for (final Field f : fieldsMap.values()) {
				if (!dbFields.contains(f.getName())) {
					continue;
				}
				if (fi > 0) {
					queryBuilder.append(",");
				}
				final String val = values.get(f.getName());
				if (val == null) {
					queryBuilder.append("NULL");
				} else {
					DatabaseUtils.appendEscapedSQLString(queryBuilder, val);
				}
				fi++;
			}
			queryBuilder.append(")");
			queries.add(queryBuilder.toString());
			maxSyncVersion = values.get("syncVersion");
		}
		in.endArray();
		in.close();
		return queries;
	}

	public String getMaxSyncVersion() {

		return maxSyncVersion;
	}
}