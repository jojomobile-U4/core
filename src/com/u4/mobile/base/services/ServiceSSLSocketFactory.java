package com.u4.mobile.base.services;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509ExtendedKeyManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.springframework.util.StringUtils;

import com.u4.mobile.base.app.MainApp;

import android.content.Context;
import android.security.KeyChain;
import android.security.KeyChainException;
import android.util.Log;

public class ServiceSSLSocketFactory extends SSLSocketFactory {

	SSLContext sslContext = SSLContext.getInstance("TLS");
	final Context context = MainApp.getInstance().getApplicationContext();
	final String alias;
	boolean serverCertRequired = false;

	final X509Certificate[] certificates;
	final PrivateKey pk;

	public ServiceSSLSocketFactory(KeyStore truststore, boolean serverCertRequired, String alias) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException,
			UnrecoverableKeyException, CertificateException {

		super(truststore);

		this.alias = alias;
		final boolean isServerCertRequired = serverCertRequired;

		certificates = getCertificateChain(alias);
		pk = getPrivateKey(alias);

		final TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(truststore);

		final TrustManager[] trustManagers = tmf.getTrustManagers();
		final X509TrustManager origTrustmanager = (X509TrustManager) trustManagers[0];
		final TrustManager tm;

		if (serverCertRequired) {

			tm = new X509TrustManager() {

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

					try {
						origTrustmanager.checkServerTrusted(chain, authType);
					} catch (final CertificateException certEx) {
						Log.e("CertificateException", certEx.getLocalizedMessage());
						if (isServerCertRequired) {
							throw new CertificateException(certEx);
						}
					} catch (final IllegalArgumentException argEx) {
						Log.e("IllegalArgumentException", argEx.getLocalizedMessage());
					}
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {

					return null;
				}
			};
		} else {
			tm = new X509TrustManager() {

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {

					return null;
				}
			};
		}

		sslContext.init(new KeyManager[] { keyManager }, new TrustManager[] { tm }, null);
	}

	X509ExtendedKeyManager keyManager = new X509ExtendedKeyManager() {

		@Override
		public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {

			return alias;
		}

		@Override
		public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {

			return alias;
		}

		@Override
		public X509Certificate[] getCertificateChain(String alias) {

			try {
				return KeyChain.getCertificateChain(context, alias);
			} catch (final KeyChainException e) {
				e.printStackTrace();
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public String[] getClientAliases(String keyType, Principal[] issuers) {

			return new String[] { alias };
		}

		@Override
		public PrivateKey getPrivateKey(String s) {

			return pk;
		}

		@Override
		public String[] getServerAliases(String keyType, Principal[] issuers) {

			return new String[] { alias };
		}

	};

	private X509Certificate[] getCertificateChain(String alias) {

		try {
			if (StringUtils.hasText(alias)) {
				return KeyChain.getCertificateChain(context, alias);
			}
		} catch (final KeyChainException e) {
			e.printStackTrace();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	private PrivateKey getPrivateKey(String alias) {

		try {
			if (StringUtils.hasText(alias)) {
				return KeyChain.getPrivateKey(context, alias);
			}
		} catch (final KeyChainException e) {
			e.printStackTrace();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Socket createSocket() throws IOException {

		return sslContext.getSocketFactory().createSocket();
	}

	@Override
	public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {

		return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
	}
}
