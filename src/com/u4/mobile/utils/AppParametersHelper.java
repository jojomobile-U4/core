package com.u4.mobile.utils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.OrmLiteDao;

import com.j256.ormlite.dao.Dao;
import com.u4.mobile.model.ApplicationParameter;
import com.u4.mobile.model.DatabaseHelper;

@EBean(scope = Scope.Singleton)
public class AppParametersHelper {

	public static final String CUSTOMER_CLEARING_CONTEXT = "MB_ACCOUNTS_KONR_CONTEXT";
	public static final String TAKE_OVER_LOAD_AVAILABLE = "MB_TR_TAKE_OVER_LOAD";
	public static final String CONFIRM_LOAD_MANUALLY = "MB_TR_CONFIRM_LOAD_MANUALLY";

	@OrmLiteDao(helper = DatabaseHelper.class, model = ApplicationParameter.class)
	Dao<ApplicationParameter, Long> appParametersDao;

	private HashMap<String, ApplicationParameter> appParametersList;
	private boolean initialized = false;

	public ApplicationParameter getParameter(String code) {
		if (!initialized) {
			loadData();
		}

		if (appParametersList.containsKey(code)) {
			return appParametersList.get(code);
		}
		return null;
	}

	public void refresh() {
		appParametersList = null;
		loadData();
	}

	private void loadData() {
		try {
			if (appParametersList == null) {
				appParametersList = new HashMap<String, ApplicationParameter>();

				final List<ApplicationParameter> appParametersListTmp = appParametersDao.queryForAll();

				for (final ApplicationParameter param : appParametersListTmp) {
					if (!appParametersList.containsKey(param.getCode())) {
						appParametersList.put(param.getCode(), param);
					}
				}

				initialized = true;

			}
		} catch (final SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), e);
		}
	}
}
