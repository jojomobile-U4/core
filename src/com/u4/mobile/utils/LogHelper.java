package com.u4.mobile.utils;

import org.springframework.util.StringUtils;

import android.util.Log;

import com.u4.mobile.base.app.ApplicationEnums;

public class LogHelper {

	public static void setDebugMessage(String tag, String message) {
		if (!StringUtils.hasText(message)) {
			message = ApplicationEnums.MessageEquivalent.NoMessage.getValue();
		}

		Log.d(tag, message);
	}

	public static void setErrorMessage(String tag, String message) {
		Log.e(tag, message);
	}

	public static void setErrorMessage(String tag, String message, StackTraceElement[] stackTrace) {
		Log.e(tag, message + " method name : " + stackTrace[0].getMethodName() + " line number : " + stackTrace[0].getLineNumber() + " stack : " + stackTrace.toString());
	}

	public static void setErrorMessage(String tag, Throwable e) {
		if (e != null) {
			if (e.getCause() != null && e.getMessage() != null) {
				Log.e(tag, e.getCause() + "\\" + e.getMessage());
			} else {
				Log.e(tag, e.toString());
			}
			e.printStackTrace();
		} else {
			Log.e(tag, "NULL EXCEPTION");
		}
	}
}