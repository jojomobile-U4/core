package com.u4.mobile.utils;

import java.util.HashMap;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;

import com.u4.mobile.R;
import com.u4.mobile.base.app.ApplicationEnums.Fonts;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

@EBean(scope = Scope.Singleton)
public class FontHelper {

	@RootContext
	static Context context;

	private static HashMap<Fonts, Typeface> fonts = new HashMap<Fonts, Typeface>();

	public static Typeface ParseFontAttributes(AttributeSet attrs) {
		TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.textViewRoboto);
		Typeface result = ParseFontAttributes(context, Fonts.values()[values.getInt(0, 0)]);
		values.recycle();
		return result;
	}

	public static Typeface ParseFontAttributes(Context context, Fonts font) {

		if (fonts.containsKey(font)) {
			return fonts.get(font);
		}

		Typeface typeface;

		switch (font) {
		case Roboto_ThinItalic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-ThinItalic.ttf");
			break;
		case Roboto_Thin:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Thin.ttf");
			break;
		case Roboto_Regular:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Regular.ttf");
			break;
		case Roboto_MediumItalic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-MediumItalic.ttf");
			break;
		case Roboto_Medium:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Medium.ttf");
			break;
		case Roboto_LightItalic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-LightItalic.ttf");
			break;
		case Roboto_Light:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Light.ttf");
			break;
		case Roboto_Italic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Italic.ttf");
			break;
		case Roboto_CondensedItalic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-CondensedItalic.ttf");
			break;
		case Roboto_Condensed:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Condensed.ttf");
			break;
		case Roboto_BoldItalic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-BoldItalic.ttf");
			break;
		case Roboto_BoldCondensedItalic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-BoldCondensedItalic.ttf");
			break;
		case Roboto_BoldCondensed:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-BoldCondensed.ttf");
			break;
		case Roboto_Bold:
		default:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Bold.ttf");
			break;
		case Roboto_BlackItalic:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-BlackItalic.ttf");
			break;
		case Roboto_Black:
			typeface = Typeface.createFromAsset(context.getAssets(), "fonts/roboto/Roboto-Black.ttf");
			break;
		}

		fonts.put(font, typeface);
		return typeface;

	}
}
