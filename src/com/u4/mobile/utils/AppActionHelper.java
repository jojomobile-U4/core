package com.u4.mobile.utils;

import org.springframework.util.StringUtils;

import com.u4.mobile.R;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class AppActionHelper {

	public static void Call(String number, Context context) {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + number));
		context.startActivity(callIntent);
	}

	public static void SendEmail(String to, String subject, String message,
			Context context) {

		if (!StringUtils.hasText(to)) {
			throw new IllegalArgumentException();
		}

		if (!StringUtils.hasText(subject)) {
			subject = context.getResources().getString(
					R.string.app_action__email_empty_subject);
		}

		if (!StringUtils.hasText(message)) {
			message = context.getResources().getString(
					R.string.app_action__email_empty_message);
		}

		Intent email = new Intent(Intent.ACTION_SEND);
		email.putExtra(Intent.EXTRA_EMAIL, new String[] { to });
		email.putExtra(Intent.EXTRA_SUBJECT, subject);
		email.putExtra(Intent.EXTRA_TEXT, message);
		email.setType("message/rfc822");

		context.startActivity(Intent.createChooser(email,
				"Choose an Email client :"));
	}

	public static void SendSMS(String phoneNumber, Context context) {

		if (!StringUtils.hasText(phoneNumber)) {
			throw new IllegalArgumentException();
		}

		context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts(
				"sms", phoneNumber, null)));
	}
}
