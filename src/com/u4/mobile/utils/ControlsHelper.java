package com.u4.mobile.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.util.StringUtils;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.u4.mobile.R;
import com.u4.mobile.base.app.MainApp;
import com.u4.mobile.base.ui.RequiredFieldValidator;
import com.u4.mobile.model.DaoHelper;
import com.u4.mobile.model.DaoManager;
import com.u4.mobile.model.DownloadTransferObjectBase;

public class ControlsHelper {
	public static void addNonZeroValueValidation(final TextView view) {
		final RequiredFieldValidator validator = new RequiredFieldValidator(view, true);
		view.addTextChangedListener(validator);
		view.setOnFocusChangeListener(validator);
	}

	public static void addRequiredFieldValidation(final TextView view) {
		final RequiredFieldValidator validator = new RequiredFieldValidator(view, false);
		view.addTextChangedListener(validator);
		view.setOnFocusChangeListener(validator);
	}

	public static void addRequiredFieldValidation(final TextView view, RequiredFieldValidator validator) {
		view.addTextChangedListener(validator);
		view.setOnFocusChangeListener(validator);
	}

	public static void addSpinnerNotEmptyValidation(Spinner vatRateSpinner, final TextView textView) {
		vatRateSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (StringUtils.hasText(arg0.getSelectedItem().toString())) {
					textView.setError(textView.getResources().getString(R.string.required_field));
				} else {
					textView.setError(null);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				textView.setError(textView.getResources().getString(R.string.required_field));
			}
		});
	}

	public static <T> boolean existsOverlappingPeriod(Class<T> daoClass, String guid, String dateFromField, String dateToField, long dateFrom, long dateTo, String filter) {
		final QueryBuilder<T, Long> qb = DaoManager.lookupDao(daoClass).queryBuilder();
		final Map<String, Object> queryParam = new HashMap<String, Object>();
		queryParam.put("dateFromField", dateFromField);
		queryParam.put("dateToField", dateToField);
		queryParam.put("dateFrom", dateFrom);
		queryParam.put("dateTo", dateTo);
		queryParam.put("guid", guid);
		String condition = DaoHelper.formatQuery("((%(dateFromField) <= %(dateFrom) AND %(dateToField) > %(dateFrom)) "
				+ "OR (%(dateFromField) < %(dateTo) AND %(dateToField) >= %(dateTo)) "
				+ "OR (%(dateFromField) > %(dateFrom) AND %(dateToField) < %(dateTo))) AND GUID <> '%(guid)'", queryParam);

		if (!StringUtils.hasText(filter)) {
			condition += filter;
		}

		qb.where().raw(condition);
		qb.setCountOf(true);
		String[] res = null;

		try {
			res = qb.queryRawFirst();
		} catch (final SQLException e) {
			LogHelper.setErrorMessage(ControlsHelper.class.getName(), e);
		}

		return res != null && !res[0].equals("0");
	}

	public static String getSearchStringAlt(String input) {

		if (!StringUtils.hasText(input)) {
			return null;
		}

		final String inputUpper = input.toUpperCase(Locale.getDefault());
		final StringBuilder stringBuilder = new StringBuilder(inputUpper.length());

		for (int i = 0; i < inputUpper.length(); i++) {
			switch (inputUpper.charAt(i)) {
			case '�':
				stringBuilder.append('E');
				break;
			case '�':
				stringBuilder.append('O');
				break;
			case '�':
				stringBuilder.append('A');
				break;
			case '�':
				stringBuilder.append('S');
				break;
			case '�':
				stringBuilder.append('L');
				break;
			case '�':
			case '�':
				stringBuilder.append('Z');
				break;
			case '�':
				stringBuilder.append('C');
				break;
			case '�':
				stringBuilder.append('N');
				break;
			default:
				stringBuilder.append(inputUpper.charAt(i));
				break;
			}
		}

		return stringBuilder.toString().toUpperCase(Locale.getDefault());
	}

	public static SpannableStringBuilder highlightText(String inputText, String highlightText) {
		String altText = getSearchStringAlt(inputText);

		final SpannableStringBuilder spannable = new SpannableStringBuilder(inputText);

		if (StringUtils.hasText(highlightText) && StringUtils.hasText(altText) && altText.toUpperCase(Locale.getDefault()).contains(highlightText.toUpperCase(Locale.getDefault()))) {
			final int start = altText.toLowerCase(Locale.getDefault()).indexOf(highlightText.toLowerCase(Locale.getDefault()));
			final int stop = start + highlightText.length();
			spannable.setSpan(new BackgroundColorSpan(MainApp.getInstance().getResources().getColor(R.color.list_highlight)), start, stop, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
		}

		return spannable;
	}

	public static void highlightText(final TextView textView, String highlightText) {
		textView.setText(highlightText(textView.getText().toString(), highlightText));
	}

	public static boolean validateRequiredFields(DownloadTransferObjectBase object) {
		final Field[] fields = object.getClass().getDeclaredFields();
		for (final Field f : fields) {
			final DatabaseField df = f.getAnnotation(DatabaseField.class);

			if (!f.getType().isPrimitive() && df != null && !df.canBeNull()) {
				try {
					final Method m = object.getClass().getMethod("get" + f.getName().substring(0, 1).toUpperCase(Locale.getDefault()) + f.getName().substring(1), new Class<?>[0]);
					if (m != null) {
						final Object obj = m.invoke(object);
						if (obj instanceof String && StringUtils.hasText((String) obj)) {
							return false;
						} else if (obj == null) {
							return false;
						}
					}
				} catch (final IllegalArgumentException e) {
					LogHelper.setErrorMessage(ControlsHelper.class.getName(), e);
				} catch (final SecurityException e) {
					LogHelper.setErrorMessage(ControlsHelper.class.getName(), e);
				} catch (final NoSuchMethodException e) {
					LogHelper.setErrorMessage(ControlsHelper.class.getName(), e);
				} catch (final IllegalAccessException e) {
					LogHelper.setErrorMessage(ControlsHelper.class.getName(), e);
				} catch (final InvocationTargetException e) {
					LogHelper.setErrorMessage(ControlsHelper.class.getName(), e);
				}
			}
		}
		return true;
	}
}
