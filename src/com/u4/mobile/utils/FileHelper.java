package com.u4.mobile.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import com.u4.mobile.model.DatabaseHelper;

import android.content.Context;

public class FileHelper {

	public static boolean writeBytesToFile(File theFile, byte[] bytes) throws IOException {
		BufferedOutputStream bos = null;

		try {
			FileOutputStream fos = new FileOutputStream(theFile);
			bos = new BufferedOutputStream(fos);
			bos.write(bytes);
		} finally {
			if (bos != null) {
				try {
					bos.flush();
					bos.close();
					return true;
				} catch (Exception e) {
				}
			}
		}
		return false;
	}

	private static void copyDatabase(Context context, File importFile) throws Exception {
		try {
			InputStream inputStream = new FileInputStream(importFile.getAbsolutePath());
			OutputStream outputStream = null;
			File file = context.getDatabasePath(DatabaseHelper.DATABASE_NAME);
			
			if (file.exists()) {
				outputStream = new FileOutputStream(file);
			} else {
				if (file.getParentFile() != null && !file.getParentFile().mkdirs()) {
				    /* handle permission problems here */
				}
				/* either no parent directories there or we have created missing directories */
				if (file.createNewFile() || file.isFile()) {
					outputStream = new FileOutputStream(file);
				} else {
				    /* handle directory here */
				}
			}

			byte[] buffer = new byte[1024];
			int length;
			
			while ((length = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, length);
			}
			
			outputStream.flush();
			outputStream.close();
			inputStream.close();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public static boolean copyDatabaseFromAssets(Context context) {
		try {
			if(!Arrays.asList(context.getAssets().list("")).contains(DatabaseHelper.DATABASE_NAME)){
				return false;
			}
			
			InputStream inputStream = context.getAssets().open(DatabaseHelper.DATABASE_NAME);

			OutputStream outputStream = null;
			File file = context.getDatabasePath(DatabaseHelper.DATABASE_NAME);
			
			if (file.exists()) {
				file.delete();
				file.createNewFile();
				outputStream = new FileOutputStream(file);
			} else {
				if (file.getParentFile() != null && !file.getParentFile().mkdirs()) {
				    /* handle permission problems here */
				}
				/* either no parent directories there or we have created missing directories */
				if (file.createNewFile() || file.isFile()) {
					outputStream = new FileOutputStream(file);
				} else {
				    /* handle directory here */
				}
				
			}

			byte[] buffer = new byte[1024];
			int length;
			
			while ((length = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, length);
			}
			
			outputStream.flush();
			outputStream.close();
			inputStream.close();
			
			return true;
		} catch (Exception e) {
			LogHelper.setErrorMessage(FileHelper.class.getName() + " - copyDatabaseFromAssets", e);
		}
		
		return false;
	}

	public static boolean saveNewDatabase(Context context, byte[] bytes) throws Exception {
		try {
			File dbTempFile = File.createTempFile("downloadedDb", ".sqlite", context.getCacheDir());

			if (writeBytesToFile(dbTempFile, bytes)) {
				copyDatabase(context, dbTempFile);
				dbTempFile.delete();
				
				return true;
			}

		} catch (IOException e) {
			throw new Exception(e);
		}
		
		return false;
	}
	
	public static File saveAppUpdateFile(Context context, byte[] bytes) throws Exception {
		try {
			File updateAppFile = File.createTempFile("AppUpdate", ".apk", context.getCacheDir());			
			BufferedOutputStream bos = null;

			try {
				FileOutputStream fos = new FileOutputStream(updateAppFile);
				bos = new BufferedOutputStream(fos);
				bos.write(bytes);
			} finally {
				if (bos != null) {
					try {
						bos.flush();
						bos.close();
						// Change file permission to be executable
						Runtime.getRuntime().exec("chmod 666 " + updateAppFile.getAbsolutePath());
						
						return updateAppFile;
					} catch (Exception e) {
						LogHelper.setErrorMessage(FileHelper.class.getName() + " - saveUpdate", e);
					}
				}
			}
		} catch (IOException e) {
			throw new Exception(e);
		}
		
		return null;
	}
}
