package com.u4.mobile.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.util.StringUtils;

public class UrlHelper {

	public static String SUFFIX = "/";

	public static URL makeUrl(String url) {
		try {
			if (!StringUtils.hasText(url)) {
				return null;
			}
			if (!url.endsWith(SUFFIX)) {
				url += SUFFIX;
			}
			return new URL(url);
		} catch (final MalformedURLException e) {
			return null;
		}
	}
}
