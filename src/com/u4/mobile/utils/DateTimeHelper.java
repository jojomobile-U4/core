package com.u4.mobile.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.u4.mobile.R;

import android.content.Context;

public class DateTimeHelper {

	public static Date getDate(long milliseconds) {

		final Calendar cal = new GregorianCalendar();
		cal.setTimeInMillis(milliseconds);

		return cal.getTime();
	}

	public static Date getDate(String date) {

		final SimpleDateFormat dateFormat = new SimpleDateFormat(DateTimeFormat.OracleDateFormat.value, Locale.getDefault());
		try {
			return dateFormat.parse(date);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Calendar TimeReset(Calendar date) {

		if (date == null) {
			return null;
		}

		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);

		return date;
	}

	public static Calendar TimeFill(Calendar date) {

		if (date == null) {
			return null;
		}

		date.set(Calendar.HOUR_OF_DAY, 23);
		date.set(Calendar.MINUTE, 59);
		date.set(Calendar.SECOND, 59);
		date.set(Calendar.MILLISECOND, 999);

		return date;
	}

	public static String getDateAsString(Date date, DateTimeFormat format) {

		return date != null ? getDateAsString(date.getTime(), format) : null;
	}

	public static String getDateAsString(String date, DateTimeFormat format) {

		Date tmpDate = getDate(date);
		return date != null ? getDateAsString(tmpDate.getTime(), format) : null;
	}

	public static String getDateAsString(long milliseconds, DateTimeFormat format) {

		if (milliseconds == 0) {
			switch (format) {
			case DateFormat:
				return "____-__-__";
			case TimeFormat:
				return "__:__:__";
			case DateShortTimeFormat:
				return "____-__-__'T'__:__";
			case HourMinuteFormat:
				return "__:__";
			case YearMonthFormat:
				return "____-__";
			default:
				return "-";
			}
		}
		final SimpleDateFormat dateFormat = new SimpleDateFormat(format.getValue(), Locale.getDefault());
		return dateFormat.format(getDate(milliseconds));
	}

	public static long getDateAsLong(Date date) {

		final Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return cal.getTimeInMillis();
	}

	public enum DateTimeFormat {
		DateTimeFormat("yyyy-MM-dd HH:mm:ss"), DateFormat("dd MMM yyyy"), OracleDateFormat("yyyy-MM-dd"), OracleDateTimeFormat("yyyy-MM-dd HH:mm:ss"), DayMonthFormat("dd-MMM"), TimeFormat(
				"HH:mm:ss"), DateShortTimeFormat("yyyy-MM-dd'T'HH:mm"), YearMonthFormat("MMM yyyy"), HourMinuteFormat("HH:mm"), FileDateTimeFormat("yyyy-MM-dd_HH-mm-ss"), FullDateTimeFormat(
				"yyyy-MM-dd HH:mm:ss.SSSSSSS'Z'"), SaveDateTimeFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZ");

		private final String value;

		DateTimeFormat(String value) {

			this.value = value;
		}

		public String getValue() {

			return value;
		}
	}

	public static CharSequence getDaysString(Context context, int days) {

		return days == 1 ? context.getString(R.string.one_day) : context.getString(R.string.x_days, days);
	}

	public static String getCurrentDateAsString(DateTimeFormat format) {

		final SimpleDateFormat dateFormat = new SimpleDateFormat(format.getValue(), Locale.getDefault());
		return dateFormat.format(Calendar.getInstance().getTime());
	}

	public static Date getCurrentDate() {

		return Calendar.getInstance().getTime();
	}
}
