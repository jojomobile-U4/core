package com.u4.mobile.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.u4.mobile.base.app.MainApp;


public class ApplicationCacheHelper {

	public static boolean copyFileToCache(File file) throws IOException {

		File cacheDir = MainApp.getInstance().getApplicationContext().getCacheDir();
		InputStream in = new FileInputStream(file);
		OutputStream out = new FileOutputStream(cacheDir + File.separator + file.getName());
		try {
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
		} catch (Exception exception) {
			return false;
		} finally {
			in.close();
			out.close();
		}

		return true;
	}

	public static boolean removeFileFromCache(String fileName) throws IOException {

		File cacheDir = MainApp.getInstance().getApplicationContext().getCacheDir();
		File file = new File(cacheDir + File.separator + fileName);
		if (file.exists()) {
			return file.delete();
		}
		return false;
	}

	public static boolean removeFile(String path) throws IOException {

		File file = new File(path);
		if (file.exists()) {
			file.delete();
		}
		return true;
	}

	public static String getCachePath(boolean withSeparator) {

		if (withSeparator) {
			return MainApp.getInstance().getApplicationContext().getCacheDir() + File.separator;
		}
		return MainApp.getInstance().getApplicationContext().getCacheDir().toString();
	}

}
