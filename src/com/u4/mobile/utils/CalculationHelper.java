package com.u4.mobile.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.util.StringUtils;

public class CalculationHelper {

	public static double round(double value) {
		return round(value, 2);
	}

	public static double getGrossValue(final double netValue, double taxValue, int precision) {
		taxValue = CalculationHelper.round(taxValue / 100);
		double kwotaVAT = 0;
		kwotaVAT = round(netValue * taxValue, precision);
		final double grosValue = kwotaVAT + netValue;
		return round(grosValue, precision);
	}

	public static double getNetValue(final double grosValue, final double taxValueNet, int precision) {
		final double v = round(grosValue * taxValueNet / 100, precision);
		return round(grosValue - v, precision);
	}

	public static double getNumberValue(String val) {
		return !StringUtils.hasText(val) ? 0.0 : parseDouble(val);
	}

	public static double round(double d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Double.toString(d));

		/**
		 * Zwiekszenie precyzji w celu unikniecia 1 groszowych roznic w
		 * wartosciach np. round(149.7749999, 2) dawalo 149.77 zamiast 149.78
		 */
		bd = bd.setScale(decimalPlace + 1, BigDecimal.ROUND_HALF_UP);
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);

		return bd.doubleValue();
	}

	public static short parseShort(String string) {
		try {
			return string != null ? NumberFormat.getNumberInstance(Locale.getDefault()).parse(string).shortValue() : 0;
		} catch (final NumberFormatException e) {
			return 0;
		} catch (final ParseException e) {
			return 0;
		}
	}

	public static float parseFloat(String string) {
		try {
			return string != null ? NumberFormat.getNumberInstance(Locale.getDefault()).parse(string).floatValue() : 0.0f;
		} catch (final NumberFormatException e) {
			return 0.0f;
		} catch (final ParseException e) {
			return 0.0f;
		}
	}

	public static double parseDouble(String string) {
		try {
			return string != null ? NumberFormat.getNumberInstance(Locale.getDefault()).parse(string).doubleValue() : 0.0d;
		} catch (final NumberFormatException e) {
			return 0.0d;
		} catch (final ParseException e) {
			return 0.0d;
		}
	}

	public static double parseDouble2Precision(double value) {
		try {
			DecimalFormat df = new DecimalFormat("0.00");
			String formate = df.format(value);
			return df.parse(formate).doubleValue();
		} catch (final NumberFormatException e) {
			return 0.0d;
		} catch (final ParseException e) {
			return 0.0d;
		}
	}

	public static int parseInt(String string) {
		try {
			return string != null ? NumberFormat.getNumberInstance(Locale.getDefault()).parse(string).intValue() : 0;
		} catch (final NumberFormatException e) {
			return 0;
		} catch (final ParseException e) {
			return 0;
		}
	}

	public static long parseLong(String string) {
		try {
			return string != null ? NumberFormat.getNumberInstance(Locale.getDefault()).parse(string).longValue() : 0L;
		} catch (final NumberFormatException e) {
			return 0L;
		} catch (final ParseException e) {
			return 0L;
		}
	}

	public static int getPricePrecision() {
		return 2;
	}

	public static String formatPrice(double d) {
		final int precision = getPricePrecision();
		return getFormattedValue(d, precision);
	}

	public static String getFormattedValue(double d, int precision) {
		return String.format("%." + precision + "f", d);
	}

	public static int getDisplayQuantityPrecision() {
		return 3;
	}

}
