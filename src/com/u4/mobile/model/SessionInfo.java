package com.u4.mobile.model;

import com.google.gson.annotations.SerializedName;

public class SessionInfo extends DownloadTransferObjectBase {

	public static final String COMPANY_ID__FIELD_NAME = "CompanyId";
	public static final String DATABASE_USER_NAME__FIELD_NAME = "DatabaseUserName";
	public static final String IS_ADMIN__FIELD_NAME = "IsAdmin";
	public static final String IS_POWER_USER__FIELD_NAME = "IsPowerUser";
	public static final String OSBY_ID__FIELD_NAME = "OsbyId";
	public static final String OSOB_ID__FIELD_NAME = "OsobId";
	public static final String PRAC_ID__FIELD_NAME = "PracId";
	public static final String SESSION_ID__FIELD_NAME = "SessionId";
	public static final String SHOW_ONLY_MY_DATA__FIELD_NAME = "ShowOnlyMyData";
	public static final String USER_ID__FIELD_NAME = "UserId";
	public static final String USER_NAME__FIELD_NAME = "UserName";
	public static final String LAST_AUTHENTICATION_ERROR_DESCRIPTION__FIELD_NAME = "LastAuthenticationErrorDescription";

	public static final String NAME = "SessionInfo";

	private static final long serialVersionUID = 1L;

	@SerializedName(COMPANY_ID__FIELD_NAME)
	private long companyId;

	@SerializedName(DATABASE_USER_NAME__FIELD_NAME)
	private String databaseUserName;

	@SerializedName(IS_ADMIN__FIELD_NAME)
	private boolean isAdmin;

	@SerializedName(IS_POWER_USER__FIELD_NAME)
	private boolean isPowerUser;

	@SerializedName(OSBY_ID__FIELD_NAME)
	private long osbyId;

	@SerializedName(OSOB_ID__FIELD_NAME)
	private long osobId;

	@SerializedName(PRAC_ID__FIELD_NAME)
	private long pracId;

	@SerializedName(SESSION_ID__FIELD_NAME)
	private double sessionId;

	@SerializedName(SHOW_ONLY_MY_DATA__FIELD_NAME)
	private boolean showOnlyMyData;

	@SerializedName("UserId")
	private long userId;

	@SerializedName("UserName")
	private String userName;

	public SessionInfo(User user) {
		sessionId = -1;
		companyId = -1;
		isAdmin = false;
		isPowerUser = false;
		showOnlyMyData = true;
		userId = user.getId();
		userName = user.getName();
		osobId = user.getOsobId();
	}
	
	public SessionInfo() {
		sessionId = -1;
		companyId = -1;
		isAdmin = false;
		isPowerUser = false;
		showOnlyMyData = true;
		userId = -1;
		osobId = -1;
	}

	@Override
	public String getKeyColumnName() {
		return null;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getDatabaseUserName() {
		return databaseUserName;
	}

	public void setDatabaseUserName(String databaseUserName) {
		this.databaseUserName = databaseUserName;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public boolean isPowerUser() {
		return isPowerUser;
	}

	public void setPowerUser(boolean isPowerUser) {
		this.isPowerUser = isPowerUser;
	}

	public long getOsbyId() {
		return osbyId;
	}

	public void setOsbyId(long osbyId) {
		this.osbyId = osbyId;
	}

	public long getOsobId() {
		return osobId;
	}

	public void setOsobId(long osobId) {
		this.osobId = osobId;
	}

	public long getPracId() {
		return pracId;
	}

	public void setPracId(long pracId) {
		this.pracId = pracId;
	}

	public double getSessionId() {
		return sessionId;
	}

	public void setSessionId(double sessionId) {
		this.sessionId = sessionId;
	}

	public boolean isShowOnlyMyData() {
		return showOnlyMyData;
	}

	public void setShowOnlyMyData(boolean showOnlyMyData) {
		this.showOnlyMyData = showOnlyMyData;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
