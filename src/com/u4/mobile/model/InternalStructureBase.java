package com.u4.mobile.model;

import java.io.Serializable;

import com.j256.ormlite.misc.BaseDaoEnabled;
import com.u4.mobile.base.app.ApplicationEnums;

public abstract class InternalStructureBase<T, TPK> extends BaseDaoEnabled<T, TPK> implements Serializable {

	private static final long serialVersionUID = 1L;

	private transient int friendlyNameId;

	private transient String MainTableName;

	private transient long PackageSize = 1024;

	private transient String TablePrefix;

	private transient ApplicationEnums.SyncDirection SyncDirection;

	private transient boolean withDataCleaning = false;

	public abstract String getKeyColumnName();

	protected InternalStructureBase() {

	}

	public int getFriendlyNameId() {

		return friendlyNameId;
	}

	public String getMainTableName() {

		return MainTableName;
	}

	public long getPackageSize() {

		return PackageSize;
	}

	public ApplicationEnums.SyncDirection getSyncDirection() {

		return SyncDirection;
	}

	public String getTablePrefix() {

		return TablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {

		TablePrefix = tablePrefix;
	}

	protected void setFriendlyNameId(int friendlyNameId) {

		this.friendlyNameId = friendlyNameId;
	}

	protected void setMainTableName(String mainTableName) {

		MainTableName = mainTableName;
	}

	protected void setPackageSize(long packageSize) {

		PackageSize = packageSize;
	}

	protected void setSyncDirection(ApplicationEnums.SyncDirection syncDirection) {

		SyncDirection = syncDirection;
	}

	public boolean isWithDataCleaning() {

		return withDataCleaning;
	}

	public void setWithDataCleaning(boolean withDataCleaning) {

		this.withDataCleaning = withDataCleaning;
	}
}
