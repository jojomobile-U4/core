package com.u4.mobile.model;

import java.util.Map;
import java.util.Map.Entry;

public class DaoHelper {

	public static String EMPTY_GUID = "EmptyGuid";

	public static String formatQuery(String query, Map<String, Object> parameters) {
		final StringBuilder builder = new StringBuilder(query);

		for (final Entry<String, Object> entry : parameters.entrySet()) {
			int start;
			final String pattern = "%(" + entry.getKey() + ")";
			final String value = entry.getValue() != null ? entry.getValue().toString() : "";

			while ((start = builder.indexOf(pattern)) != -1) {
				builder.replace(start, start + pattern.length(), value);
			}
		}

		return builder.toString();
	}

	public static String makeLikeExpression(String value) {

		return "%" + value + "%";
	}
}
