package com.u4.mobile.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileModuleActionUser")
public class MobileModuleActionUser extends DownloadTransferObjectBase {

	public static final String MMAC_ACTION_CODE__FIELD_NAME = "MmacActionCode";
	public static final String MOUS_ID__FIELD_NAME = "MousId";	
	public static final String NAME = "MobileModuleActionUser";
	public static final String TABLE_PREFIX = "MMAU";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = MMAC_ACTION_CODE__FIELD_NAME, canBeNull = false)
	private String mmacActionCode;
	
	@DatabaseField(columnName = MOUS_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME)
	private MobileModuleUser mobileModuleUser;

}
