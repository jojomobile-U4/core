package com.u4.mobile.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ConfigurtionInfo")
public class ConfigInfo extends DownloadTransferObjectBase {

	public static final String VERSION__FIELD_NAME = "Version";
	public static final String USER_NAME__FIELD_NAME = "UserName";
	public static final String PASSWORD__FIELD_NAME = "Password";
	public static final String MAX_PACKAGE_LENGTH__FIELD_NAME = "MaxPackageLength";
	public static final String NAME = "ConfigInfo";
	public static final String TABLE_PREFIX = "ConfigInfo";
	public static final long PackageSize = 1L;

	private static final long serialVersionUID = 1L;

	@SerializedName(VERSION__FIELD_NAME)
	@DatabaseField(columnName = VERSION__FIELD_NAME, canBeNull = true)
	private String version;

	@SerializedName(USER_NAME__FIELD_NAME)
	@DatabaseField(columnName = USER_NAME__FIELD_NAME, canBeNull = true)
	private String userName;

	@SerializedName(PASSWORD__FIELD_NAME)
	@DatabaseField(columnName = PASSWORD__FIELD_NAME, canBeNull = true)
	private String password;

	@SerializedName(MAX_PACKAGE_LENGTH__FIELD_NAME)
	@DatabaseField(columnName = MAX_PACKAGE_LENGTH__FIELD_NAME, canBeNull = true, defaultValue = "0")
	private int maxPackageLength;

	public ConfigInfo() {
		super();
	}

	public ConfigInfo(String userName, String password) {
		setUserName(userName);
		setPassword(password);
	}

	public int getMaxPackageLength() {
		return maxPackageLength;
	}

	public void setMaxPackageLength(int maxPackageLength) {
		this.maxPackageLength = maxPackageLength;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
