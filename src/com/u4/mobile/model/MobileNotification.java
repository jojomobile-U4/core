package com.u4.mobile.model;

import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileNotification")
public class MobileNotification extends DownloadTransferObjectBase {
	public static enum Priority {
		L("Low", -1), N("Normal", 0), H("High", 1);

		private final String value;
		private final int platformPriority;

		Priority(String value, int platformPriority) {
			this.value = value;
			this.platformPriority = platformPriority;
		}

		public String getValue() {
			return value;
		}

		public int getPlatformPriority() {
			return platformPriority;
		}
	}

	public static final String TITLE__FIELD_NAME = "Title";
	public static final String DESCRIPTION__FIELD_NAME = "Description";
	public static final String MMOD_ID__FIELD_NAME = "MmodId";
	public static final String CREATION_DATE__FIELD_NAME = "CreationDate";
	public static final String EXPIRATION_DATE__FIELD_NAME = "ExpirationDate";
	public static final String RECEIPT_DATE__FIELD_NAME = "ReceiptDate";
	public static final String SOURCE_TABLE_ALIAS__FIELD_NAME = "SourceTableAlias";
	public static final String SOURCE_GUID__FIELD_NAME = "SourceGuid";
	public static final String SOURCE_SYNC_VERSION__FIELD_NAME = "SourceSyncVersion";
	public static final String PRIORITY__FIELD_NAME = "Priority";
	public static final String READ__FIELD_NAME = "Read";
	public static final String SYSTEM_NOTIFIED__FIELD_NAME = "SystemNotified";
	public static final String NAME = "MobileNotification";
	public static final String TABLE_PREFIX = "NOTI";

	private static final long serialVersionUID = 1L;

	@SerializedName("MmodId")
	@DatabaseField(columnName = MMOD_ID__FIELD_NAME, canBeNull = true, foreign = true, foreignColumnName = ID__FIELD_NAME)
	private MobileModule module;

	@SerializedName("CreationDate")
	@DatabaseField(columnName = CREATION_DATE__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long creationDate;

	@SerializedName("ExpirationDate")
	@DatabaseField(columnName = EXPIRATION_DATE__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long expirationDate;

	@DatabaseField(columnName = RECEIPT_DATE__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long receiptDate;

	@SerializedName("SourceTableAlias")
	@DatabaseField(columnName = SOURCE_TABLE_ALIAS__FIELD_NAME, canBeNull = true)
	private String sourceTableAlias;

	@SerializedName("SourceGuid")
	@DatabaseField(columnName = SOURCE_GUID__FIELD_NAME, canBeNull = true)
	private UUID sourceGuid;

	@SerializedName("SourceSyncVersion")
	@DatabaseField(columnName = SOURCE_SYNC_VERSION__FIELD_NAME, canBeNull = true)
	private String sourceSyncVersion;

	@SerializedName("Title")
	@DatabaseField(columnName = TITLE__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private String title;

	@SerializedName("Description")
	@DatabaseField(columnName = DESCRIPTION__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private String description;

	@SerializedName("Priority")
	@DatabaseField(columnName = PRIORITY__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private Priority priority;

	@DatabaseField(columnName = READ__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private boolean read;

	@DatabaseField(columnName = SYSTEM_NOTIFIED__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private boolean systemNotified;

	public MobileNotification() {
		super();
	}

	public MobileModule getModule() {
		return module;
	}

	public void setModule(MobileModule module) {
		this.module = module;
	}

	public long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	public long getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(long expirationDate) {
		this.expirationDate = expirationDate;
	}

	public long getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(long receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getSourceTableAlias() {
		return sourceTableAlias;
	}

	public void setSourceTableAlias(String sourceTableAlias) {
		this.sourceTableAlias = sourceTableAlias;
	}

	public UUID getSourceGuid() {
		return sourceGuid;
	}

	public void setSourceGuid(UUID sourceGuid) {
		this.sourceGuid = sourceGuid;
	}

	public String getSourceSyncVersion() {
		return sourceSyncVersion;
	}

	public void setSourceSyncVersion(String sourceSyncVersion) {
		this.sourceSyncVersion = sourceSyncVersion;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}
}
