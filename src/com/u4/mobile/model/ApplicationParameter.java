package com.u4.mobile.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ApplicationParameter")
public class ApplicationParameter extends DownloadTransferObjectBase {

	public static final String NAME__FIELD_NAME = "Name";
	public static final String CODE__FIELD_NAME = "Code";
	public static final String DESCRIPTION__FIELD_NAME = "Description";
	public static final String VALUE__FIELD_NAME = "Value";
	public static final String NAME = "ApplicationParameter";
	public static final String TABLE_PREFIX = "APPA";
	public static final String USER_ID__FIELD_NAME = "UserId";

	private static final long serialVersionUID = 1L;

	@SerializedName("Name")
	@DatabaseField(columnName = NAME__FIELD_NAME)
	private String name;

	@SerializedName("Code")
	@DatabaseField(columnName = CODE__FIELD_NAME, uniqueIndexName = "Uq_ApplicationParameter_Code_User")
	private String code;

	@SerializedName("Description")
	@DatabaseField(columnName = DESCRIPTION__FIELD_NAME)
	private String description;

	@SerializedName("Value")
	@DatabaseField(columnName = VALUE__FIELD_NAME)
	private String value;

	@DatabaseField(columnName = USER_ID__FIELD_NAME, foreign = true, canBeNull = true, index = true, uniqueIndexName = "Uq_ApplicationParameter_Code_User")
	private User user;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
