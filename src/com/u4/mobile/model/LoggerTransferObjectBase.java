package com.u4.mobile.model;


import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

public class LoggerTransferObjectBase extends DownloadTransferObjectBase {

	public transient static final String LAST_MODIFICATION_DATE_TICKS__FIELD_NAME = "LastModificationDateTicks";
	public transient static final String CREATED_BY__FIELD_NAME = "CreatedBy";
	public transient static final String CREATION_DATE_TICKS__FIELD_NAME = "CreationDateTicks";
	public transient static final String LAST_MODIFIED_BY__FIELD_NAME = "LastModifiedBy";

	private static final long serialVersionUID = 1L;

	@SerializedName(CREATED_BY__FIELD_NAME)
	@DatabaseField(columnName = CREATED_BY__FIELD_NAME, canBeNull = true)
	private String createdBy;

	@SerializedName(CREATION_DATE_TICKS__FIELD_NAME)
	@DatabaseField(columnName = CREATION_DATE_TICKS__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long creationDateTicks;

	@SerializedName(LAST_MODIFICATION_DATE_TICKS__FIELD_NAME)
	@DatabaseField(columnName = LAST_MODIFICATION_DATE_TICKS__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long lastModificationDateTicks;

	@SerializedName(LAST_MODIFIED_BY__FIELD_NAME)
	@DatabaseField(columnName = LAST_MODIFIED_BY__FIELD_NAME, canBeNull = true)
	private String lastModifiedBy;

	protected LoggerTransferObjectBase() {

		super();
	}

	public String getCreatedBy() {

		return createdBy;
	}

	public void setCreatedBy(String createdBy) {

		this.createdBy = createdBy;
	}

	public long getCreationDateTicks() {

		return creationDateTicks;
	}

	public void setCreationDateTicks(long creationDateTicks) {

		this.creationDateTicks = creationDateTicks;
	}

	public long getLastModificationDateTicks() {

		return lastModificationDateTicks;
	}

	public void setLastModificationDateTicks(long lastModificationDateTicks) {

		this.lastModificationDateTicks = lastModificationDateTicks;
	}

	public String getLastModifiedBy() {

		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {

		this.lastModifiedBy = lastModifiedBy;
	}
}
