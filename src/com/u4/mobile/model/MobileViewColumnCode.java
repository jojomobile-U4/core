package com.u4.mobile.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileViewColumnCode")
public class MobileViewColumnCode extends DownloadTransferObjectBase {

	public static final String MVIC_ID__FIELD_NAME = "MvicId";
	public static final String IZ__FIELD_NAME = "Iz";	
	public static final String CODE__FIELD_NAME = "Code";
	public static final String URI_PARAMETERS__FIELD_NAME = "UriParameters";
	public static final String NAME = "MobileViewColumnCode";
	public static final String TABLE_PREFIX = "MVIC";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = MVIC_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME, index = true)
	private MobileViewColumn mobileViewColumn;

	@DatabaseField(columnName = IZ__FIELD_NAME, canBeNull = false)
	private String iz;

	@DatabaseField(columnName = CODE__FIELD_NAME)
	private String code;

	@DatabaseField(columnName = URI_PARAMETERS__FIELD_NAME)
	private String uriParameters;
}
