package com.u4.mobile.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Company")
public class Company extends DownloadTransferObjectBase {

	public static final String NAME__FIELD_NAME = "Name";
	public static final String CODE__FIELD_NAME = "Code";
	public static final String COMPANY_ID__FIELD_NAME = "CompanyId";
	public static final String USER_ID__FIELD_NAME = "UserId";
	public static final String IS_CURRENT__FIELD_NAME = "IsCurrent";
	public static final String NAME = "Company";
	public static final String TABLE_PREFIX = "FIRU";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = COMPANY_ID__FIELD_NAME, canBeNull = false)
	private Integer companyId;

	@DatabaseField(columnName = CODE__FIELD_NAME, canBeNull = false)
	private String code;

	@DatabaseField(columnName = NAME__FIELD_NAME, canBeNull = false)
	private String name;
	
	@DatabaseField(columnName = USER_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME)
	private User user;

	@DatabaseField(columnName = IS_CURRENT__FIELD_NAME, canBeNull = false)
	private boolean isCurrent;	
}
