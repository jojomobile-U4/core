package com.u4.mobile.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileViewColumn")
public class MobileViewColumn extends DownloadTransferObjectBase {

	public static final String COLUMN_TYPE__FIELD_NAME = "ColumnType";
	public static final String MVIW_ID__FIELD_NAME = "MviwId";
	public static final String DISPLAY_NAME__FIELD_NAME = "DisplayName";	
	public static final String NAME__FIELD_NAME = "Name";
	public static final String TABLE_NAME__FIELD_NAME = "TableName";
	public static final String FILTERABLE__FIELD_NAME = "Filterable";
	public static final String REQUIRED__FIELD_NAME = "Required";
	public static final String SCANABLE__FIELD_NAME = "Scanable";
	public static final String SORTABLE__FIELD_NAME = "Sortable";
	public static final String VISIBLE__FIELD_NAME = "Visible";
	public static final String EDITABLE__FIELD_NAME = "Editable";
	public static final String SCAN_ORDER__FIELD_NAME = "ScanOrder";
	public static final String WIDTH__FIELD_NAME = "Width";
	public static final String WRAP_GRID_LINES_COUNT__FIELD_NAME = "WrapGridLinesCount";
	public static final String IS_GRID_COLUMN__FIELD_NAME = "IsGridColumn";
	public static final String COLUMN_ORDER__FIELD_NAME = "ColumnOrder";
	public static final String NAME = "MobileViewColumn";
	public static final String TABLE_PREFIX = "MVIC";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = COLUMN_TYPE__FIELD_NAME, canBeNull = false)
	private String columnType;
	
	@DatabaseField(columnName = MVIW_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME, index = true)
	private MobileView mobileView;

	@DatabaseField(columnName = DISPLAY_NAME__FIELD_NAME, canBeNull = false)
	private String displayName;

	@DatabaseField(columnName = NAME__FIELD_NAME, canBeNull = false)
	private String name;

	@DatabaseField(columnName = TABLE_NAME__FIELD_NAME)
	private String tableName;

	@DatabaseField(columnName = FILTERABLE__FIELD_NAME, canBeNull = false)
	private boolean filterable;

	@DatabaseField(columnName = REQUIRED__FIELD_NAME, canBeNull = false)
	private boolean required;

	@DatabaseField(columnName = SCANABLE__FIELD_NAME, canBeNull = false)
	private boolean scanable;

	@DatabaseField(columnName = SORTABLE__FIELD_NAME, canBeNull = false)
	private boolean sortable;

	@DatabaseField(columnName = VISIBLE__FIELD_NAME, canBeNull = false)
	private boolean visible;

	@DatabaseField(columnName = EDITABLE__FIELD_NAME, canBeNull = false)
	private boolean editable;

	@DatabaseField(columnName = SCAN_ORDER__FIELD_NAME)
	private long scanOrder;
	
	@DatabaseField(columnName = WIDTH__FIELD_NAME, canBeNull = true)
	private long width;
	
	@DatabaseField(columnName = WRAP_GRID_LINES_COUNT__FIELD_NAME)
	private long wrapGridLinesCount;

	@DatabaseField(columnName = IS_GRID_COLUMN__FIELD_NAME)
	private boolean isGridColumn;

	@DatabaseField(columnName = COLUMN_ORDER__FIELD_NAME)
	private long columnOrder;
}
