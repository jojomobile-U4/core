package com.u4.mobile.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileModuleUser")
public class MobileModuleUser extends DownloadTransferObjectBase {
	public static final String MMOD_ID__FIELD_NAME = "MmodId";
	public static final String USER_ID__FIELD_NAME = "UserId";
	public static final String TABLE_PREFIX = "MOUS";
	public static final String NAME = "MobileModuleUser";

	public static final long serialVersionUID = 1L;

	@SerializedName("MmodId")
	@DatabaseField(columnName = MMOD_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME)
	private MobileModule mobileModule;

	@SerializedName("UserId")
	@DatabaseField(columnName = USER_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME)
	private User user;

	// public MobileModuleUser() {
	// super(NAME, R.string.structure__mobile_module_user__friendly_name,
	// TABLE_PREFIX, SyncDirection.Download);
	// }
	//
	// public MobileModuleUser(Long id) {
	// this();
	// setId(id);
	// }

	public MobileModule getMobileModule() {
		return mobileModule;
	}

	public void setMobileModule(MobileModule mobileModule) {
		this.mobileModule = mobileModule;
	}

	// private static List<MobileModule> userModules;
	// private static List<ModuleCode> userModuleCodes;
	//
	// public static List<MobileModule> getUserModules() throws SQLException {
	// if (userModules == null || userModuleCodes == null) {
	// final QueryBuilder<MobileModuleUser, Long> mousQb =
	// DaoManager.lookupDao(MobileModuleUser.class).queryBuilder();
	// userModules =
	// DaoManager.lookupDao(MobileModule.class).queryBuilder().join(mousQb).orderBy(MobileModule.POSITION__FIELD_NAME,
	// true).query();
	//
	// userModuleCodes = new ArrayList<ModuleCode>();
	// for (final MobileModule module : userModules) {
	// userModuleCodes.add(module.getCode());
	// }
	// }
	// return userModules;
	// }
	//
	// public static boolean isModuleAllowed(ModuleCode code) {
	// if (userModuleCodes == null) {
	// try {
	// getUserModules();
	// } catch (SQLException e) {
	// LogHelper.setErrorMessage(MobileModuleUser.class.getName(), e);
	// return false;
	// }
	// }
	// return userModuleCodes.contains(code);
	// }
	//
	// public static void resetUserModules() {
	// userModules = null;
	// userModuleCodes = null;
	// }
}
