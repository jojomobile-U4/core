package com.u4.mobile.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileViewAction")
public class MobileViewAction extends DownloadTransferObjectBase {

	public static final String ACTIVE__FIELD_NAME = "Active";
	public static final String BEFORE__FIELD_NAME = "Before";
	public static final String CODE__FIELD_NAME = "Code";
	public static final String MVIW_ID__FIELD_NAME = "MviwId";
	public static final String URI_PARAMETERS__FIELD_NAME = "UriParameters";
	public static final String NAME = "MobileViewAction";
	public static final String TABLE_PREFIX = "MVAC";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = ACTIVE__FIELD_NAME, canBeNull = false)
	private Boolean active;
	
	@DatabaseField(columnName = BEFORE__FIELD_NAME, canBeNull = false)
	private Boolean before;
	
	@DatabaseField(columnName = CODE__FIELD_NAME, canBeNull = false)
	private String code;
	
	@DatabaseField(columnName = MVIW_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME)
	private MobileView mobileView;

	@DatabaseField(columnName = URI_PARAMETERS__FIELD_NAME, canBeNull = false)
	private String uriParameters;
}
