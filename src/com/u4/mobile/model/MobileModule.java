package com.u4.mobile.model;

import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileModule")
public class MobileModule extends DownloadTransferObjectBase {
	public static final String CODE__FIELD_NAME = "code";
	public static final String NAME__FIELD_NAME = "name";
	public static final String POSITION__FIELD_NAME = "Position";
	public static final String SYSTEM__FIELD_NAME = "System";
	public static final String CONTROLLER_GUID__FIELD_NAME = "ControllerGuid";
	public static final String TABLE_PREFIX = "MMOD";
	public static final String NAME = "MobileModule";

	private static final long serialVersionUID = 1L;

	@SerializedName("Code")
	@DatabaseField(columnName = CODE__FIELD_NAME, canBeNull = true)
	private String code;

	@SerializedName("Name")
	@DatabaseField(columnName = NAME__FIELD_NAME, canBeNull = false)
	private String name;

	@SerializedName("Position")
	@DatabaseField(columnName = POSITION__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long position;

	@SerializedName("System")
	@DatabaseField(columnName = SYSTEM__FIELD_NAME, canBeNull = false)
	private String system;

	@SerializedName("ControllerGuid")
	@DatabaseField(columnName = CONTROLLER_GUID__FIELD_NAME, canBeNull = false)
	private UUID controllerGuid;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPosition() {
		return position;
	}

	public void setPosition(long position) {
		this.position = position;
	}
}