package com.u4.mobile.model;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.u4.mobile.utils.LogHelper;

public class DaoManager extends com.j256.ormlite.dao.DaoManager {

	public static <T> Dao<T, Long> lookupDao(Class<T> arg) {

		try {
			Dao<T, Long> dao = lookupDao(DatabaseHolder.getInstance().getConnectionSource(), arg);

			if (dao == null) {
				dao = createDao(DatabaseHolder.getInstance().getConnectionSource(), arg);
			}
			return dao;

		} catch (final SQLException e) {
			LogHelper.setErrorMessage(DaoManager.class.getName(), e);
			e.printStackTrace();
		}

		return null;
	}

	public static <T extends DownloadTransferObjectBase> T queryForGuid(String guid, Class<T> clazz) throws SQLException {
		return queryForGuid(guid, DaoManager.lookupDao(clazz));
	}

	public static <T extends DownloadTransferObjectBase> T queryForGuid(String guid, Dao<T, Long> dao) throws SQLException {
		List<T> results = dao.queryForEq(DownloadTransferObjectBase.GUID__FIELD_NAME, guid);

		if (results != null && results.size() > 0) {
			return results.get(0);
		}

		return null;
	}

}
