package com.u4.mobile.model;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MobileView")
public class MobileView extends DownloadTransferObjectBase {
	public static final String MMOD_ID__FIELD_NAME = "MmodId";
	public static final String NAME__FIELD_NAME = "Name";
	public static final String NAMESPACE__FIELD_NAME = "Namespace";
	public static final String SEQUENCE__FIELD_NAME = "Sequence";
	public static final String CONTROLLER_GUID__FIELD_NAME = "ControllerGuid";
	public static final String GLOBAL_SCAN_CODE__FIELD_NAME = "GlobalScanCode";
	public static final String AUTH_AVAILABLE__FIELD_NAME = "AuthAvailable";
	public static final String TABLE_PREFIX = "MVIW";
	public static final String NAME = "MobileView";

	public static final long serialVersionUID = 1L;

	@SerializedName("MmodId")
	@DatabaseField(columnName = MMOD_ID__FIELD_NAME, canBeNull = false, foreign = true, foreignColumnName = ID__FIELD_NAME, index = true)
	private MobileModule mobileModule;

	@SerializedName("Name")
	@DatabaseField(columnName = NAME__FIELD_NAME, canBeNull = false)
	private String name;

	@SerializedName("Namespace")
	@DatabaseField(columnName = NAMESPACE__FIELD_NAME, canBeNull = false)
	private String namespace;

	@SerializedName("Sequence")
	@DatabaseField(columnName = SEQUENCE__FIELD_NAME, canBeNull = false, defaultValue = "0")
	private long sequence;

	@DatabaseField(columnName = CONTROLLER_GUID__FIELD_NAME, canBeNull = false, index = true)
	private UUID controllerGuid;

	@DatabaseField(columnName = GLOBAL_SCAN_CODE__FIELD_NAME)
	private String globalScanCode;
	
	@SerializedName("AuthAvailable")
	@DatabaseField(columnName = AUTH_AVAILABLE__FIELD_NAME, canBeNull = false)
	private boolean authAvailable;

	public MobileModule getMobileModule() {
		return mobileModule;
	}

	public void setMobileModule(MobileModule mobileModule) {
		this.mobileModule = mobileModule;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public long getSequence() {
		return sequence;
	}

	public void setSequence(long sequence) {
		this.sequence = sequence;
	}

	public static List<MobileView> getViewsForModule(MobileModule module) throws SQLException {
		return DaoManager.lookupDao(MobileView.class).queryBuilder().orderBy(SEQUENCE__FIELD_NAME, true)
				.where().eq(MobileView.MMOD_ID__FIELD_NAME, module.getId()).query();
	}
}
