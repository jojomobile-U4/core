package com.u4.mobile.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.u4.mobile.base.app.Version;

public class UpdateInfo implements Serializable {

	public static final String FILE_NAME__FIELD_NAME = "FileName";
	public static final String REQUIRED__FIELD_NAME = "Required";
	public static final String VERSION__FIELD_NAME = "Version";
	private static final long serialVersionUID = 1L;

	@SerializedName(FILE_NAME__FIELD_NAME)
	private String fileName;

	@SerializedName(REQUIRED__FIELD_NAME)
	private boolean required;

	@SerializedName(VERSION__FIELD_NAME)
	private String version;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public Version getVersion() {
		return new Version(version);
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
