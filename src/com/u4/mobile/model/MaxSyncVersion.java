package com.u4.mobile.model;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "MaxSyncVersion")
public class MaxSyncVersion extends InternalStructureBase<MaxSyncVersion, Long> {
	public static final String ID = "Id";
	public static final String TABLE_NAME = "tableName";
	public static final String OBJECT_ALIAS = "objectAlias";
	public static final String SYNC_VERSION = "SyncVersion";
	public static final String USER_ID = "UserId";
	public static final String NAME = "MaxSyncVersion";
	public static final String TABLE_PREFIX = "MSV";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = ID, canBeNull = false, generatedId = true)
	private int id;

	@DatabaseField(columnName = TABLE_NAME, canBeNull = false, uniqueCombo = true)
	private String tableName;

	@DatabaseField(columnName = OBJECT_ALIAS, canBeNull = false, defaultValue = "-", uniqueCombo = true)
	private String objectAlias;

	@DatabaseField(columnName = SYNC_VERSION, canBeNull = false, defaultValue = "0")
	private String syncVersion;

	@DatabaseField(columnName = USER_ID, canBeNull = false, defaultValue = "0", uniqueCombo = true)
	private long userId;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getObjectAlias() {
		return objectAlias;
	}

	public void setObjectAlias(String objectAlias) {
		this.objectAlias = objectAlias;
	}

	public static String getMaxSyncVersion(String tableName) throws SQLException {
		return getMaxSyncVersion(tableName, "-");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSyncVersion() {
		return syncVersion;
	}

	public void setSyncVersion(String syncVersion) {
		this.syncVersion = syncVersion;
	}

	public static String getMaxSyncVersion(String tableName, String objectAlias) throws SQLException {
		List<MaxSyncVersion> list = DaoManager.lookupDao(MaxSyncVersion.class).queryBuilder().where().eq(TABLE_NAME, tableName).and().eq(OBJECT_ALIAS, objectAlias).query();
		return list == null || list.size() == 0 ? "0" : list.get(0).getSyncVersion();
	}

	public static void setMaxSyncVersion(String tableName, String objectAlias, String maxSyncVersion) throws SQLException {
		final String query = "INSERT OR REPLACE INTO " + NAME + " (" + TABLE_NAME + ", " + OBJECT_ALIAS + ", " + SYNC_VERSION + ") VALUES  " + "('" + tableName + "', '"
				+ (objectAlias == null ? "-" : objectAlias) + "', '" + (maxSyncVersion == null ? "0" : maxSyncVersion) + "')";
		DaoManager.lookupDao(MaxSyncVersion.class).executeRawNoArgs(query);
	}

	@Override
	public String getKeyColumnName() {
		return ID;
	}
}
