package com.u4.mobile.model;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.File;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.u4.mobile.base.app.MainApp;
import com.u4.mobile.utils.LogHelper;

// TODO zmieni� na @EBean klase i doda� @RootContext
public class DatabaseHolder extends OrmLiteSqliteOpenHelper {
	private static Context context;
	private static DatabaseHolder instance = null;

	public static DatabaseHolder getInstance() {
		context = MainApp.getInstance().getApplicationContext();

		if (instance == null || instance.getConnectionSource() == null || !instance.isOpen()) {
			instance = new DatabaseHolder(context, DatabaseHelper.DATABASE_NAME, DatabaseHelper.DATABASE_VERSION);

			if (instance != null) {
				instance.getWritableDatabase();
			}
		}

		return instance;
	}

	public static DatabaseHolder recreateInstance() {
		context = MainApp.getInstance().getApplicationContext();
		instance = new DatabaseHolder(context, DatabaseHelper.DATABASE_NAME, DatabaseHelper.DATABASE_VERSION);

		if (instance != null) {
			instance.getWritableDatabase();
		}

		return instance;
	}

	public DatabaseHolder(Context context, final String databaseName, final int databaseVersion) {
		super(context, databaseName, null, databaseVersion);
		
		DatabaseHolder.context = context;
		instance = this;
	}

	@Override
	public void onCreate(android.database.sqlite.SQLiteDatabase db, ConnectionSource ConnectionSource) {
		try {
			Log.i(DatabaseHolder.class.getName(), "onCreate");

			// createTable(ConfigInfo.class);

		} catch (final SQLException e) {
			Log.e(DatabaseHolder.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void close() {
		super.close();

		if (getConnectionSource() != null) {
			getConnectionSource().closeQuietly();
		}

		com.j256.ormlite.dao.DaoManager.clearCache();
	}

	@Override
	public void onUpgrade(android.database.sqlite.SQLiteDatabase db, ConnectionSource ConnectionSource, int oldVersion, int newVersion) {
		Log.i(DatabaseHolder.class.getName(), "onUpdate");

		// recreateTable(ConfigInfo.class);
	}

	public <T> void dropTable(Class<T> table) {
		try {
			TableUtils.dropTable(getConnectionSource(), table, true);
		} catch (final java.sql.SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), "Can't drop table :" + table.getName(), e.getStackTrace());
		}
	}

	public <T> void createTable(Class<T> table) {
		try {
			TableUtils.createTable(getConnectionSource(), table);
		} catch (final java.sql.SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), "Can't create table :" + table.getName(), e.getStackTrace());
		}
	}

	public <T> void recreateTable(Class<T> table) {
		try {
			TableUtils.dropTable(getConnectionSource(), table, true);
			TableUtils.createTable(getConnectionSource(), table);
		} catch (final java.sql.SQLException e) {
			LogHelper.setErrorMessage(this.getClass().getName(), "Can't recreate table :" + table.getName(), e.getStackTrace());
		}
	}

	public void recreateDb() {
		if (instance.getWritableDatabase() != null) {
			onUpgrade(instance.getWritableDatabase(), getConnectionSource(), DatabaseHelper.DATABASE_VERSION, DatabaseHelper.DATABASE_VERSION);
		}
	}
	
	public static boolean openDatabase(Context context) {
		boolean success = false;
		SQLiteDatabase database = null;

		try {
			File databaseFile = context.getDatabasePath(DatabaseHelper.DATABASE_NAME);
			
			if (databaseFile.exists()) {
				database = SQLiteDatabase.openDatabase(databaseFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);

				final Cursor c = database.rawQuery(String.format("SELECT COUNT(1) FROM %s", User.NAME), null);

				if (c.moveToFirst()) {
					success = true;
				}

				c.close();
			}
		} catch (final SQLiteException e) {
			LogHelper.setErrorMessage(DatabaseHolder.class.getName(), "database does't exist yet.");
		} finally {
			if (database != null) {
				database.close();
			}
		}

		return success;
	}
	
	public static boolean checkDatabaseVersion(Context context) {
		if(checkDatabase(context)) {
			SQLiteDatabase database = SQLiteDatabase.openDatabase(context.getDatabasePath(DatabaseHelper.DATABASE_NAME).getPath(), null, SQLiteDatabase.OPEN_READONLY);
			int version = database.getVersion();
			
			if(version < DatabaseHelper.DATABASE_VERSION && version != 0) {
				return false;
			}
			
			return true;
		}
		
		return false;
	}

	public static boolean checkDatabase(Context context) {
		boolean success = false;

		try {
			success = openDatabase(context);
		} catch (final Exception e) {
			LogHelper.setErrorMessage(DatabaseHolder.class.getName(), "database does't exist yet.");
		}

		return success;
	}
}
