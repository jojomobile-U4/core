package com.u4.mobile.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "User")
public class User extends DownloadTransferObjectBase {

	public static final String NAME__FIELD_NAME = "Name";
	public static final String PASSWORD_HASH__FIELD_NAME = "PasswordHash";
	public static final String OSOB_ID__FIELD_NAME = "OsobId";
	public static final String NAME = "User";
	public static final String TABLE_PREFIX = "USR";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = NAME__FIELD_NAME, canBeNull = false, unique = true)
	private String name;

	@DatabaseField(columnName = PASSWORD_HASH__FIELD_NAME, canBeNull = false)
	private String passwordHash;

	@SerializedName(OSOB_ID__FIELD_NAME)
	@DatabaseField(columnName = OSOB_ID__FIELD_NAME, canBeNull = true)
	private long osobId;

	public String getName() {
		return name;
	}

	public long getOsobId() {
		return osobId;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOsobId(long osobId) {
		this.osobId = osobId;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
}
