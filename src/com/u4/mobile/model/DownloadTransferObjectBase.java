package com.u4.mobile.model;

import java.sql.SQLException;
import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.u4.mobile.base.app.ApplicationEnums.RecordStates;

public class DownloadTransferObjectBase extends InternalStructureBase<DownloadTransferObjectBase, Long> {

	public transient static final String ID__FIELD_NAME = "Id";
	public transient static final String GUID__FIELD_NAME = "Guid";
	public transient static final String RECORD_STATE__FIELD_NAME = "RecordState";

	private static final long serialVersionUID = 1L;

	@SerializedName(ID__FIELD_NAME)
	@DatabaseField(columnName = ID__FIELD_NAME, generatedId = true, allowGeneratedIdInsert = true)
	private transient Long id;

	@SerializedName(GUID__FIELD_NAME)
	@DatabaseField(columnName = GUID__FIELD_NAME, canBeNull = true)
	private String guid;

	@SerializedName(RECORD_STATE__FIELD_NAME)
	@DatabaseField(columnName = RECORD_STATE__FIELD_NAME, canBeNull = true, defaultValue = "0")
	private int recordState;

	protected DownloadTransferObjectBase() {

		super();
		setGuid(UUID.randomUUID().toString());
	}

	public String getGuid() {

		return guid;
	}

	public Long getId() {

		return id;
	}

	@Override
	public String getKeyColumnName() {

		return ID__FIELD_NAME;
	}

	public RecordStates getRecordState() {
		return RecordStates.values()[recordState];
	}

	public int getRecordStateValue() {

		return recordState;
	}

	public void setGuid(String guid) {

		this.guid = guid;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public void setRecordState(RecordStates recordState) {
		this.recordState = recordState.intValue();
	}

	public void setRecordStateValue(int recordState) {

		this.recordState = recordState;
	}

	public int update(RecordStates recordStates) throws SQLException {
		setRecordState(recordStates);

		return super.update();
	}
}
