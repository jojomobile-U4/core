package com.u4.mobile.model;

import com.google.gson.annotations.SerializedName;

public class Trash extends DownloadTransferObjectBase {
	public static final String NAME = "Trash";
	public static final String TABLE_PREFIX = "TRASH";
	public static final String ALIAS__FIELD_NAME = "Alias";
	public static final String REF_ID__FIELD_NAME = "RefId";

	private static final long serialVersionUID = 1L;

	@SerializedName("Alias")
	private String alias;

	@SerializedName("RefId")
	private int refId;

	public Trash() {
		super();
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public int getRefId() {
		return refId;
	}

	public void setRefId(int refId) {
		this.refId = refId;
	}
}
