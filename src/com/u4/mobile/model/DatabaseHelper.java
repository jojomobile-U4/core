package com.u4.mobile.model;

import java.io.File;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.EBean.Scope;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.u4.mobile.utils.FileHelper;

@EBean(scope = Scope.Singleton)
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	public static final int DATABASE_VERSION = 34;
	public static final String DATABASE_NAME = "u4mobile.sqlite";
	private boolean isVersionConsistent = true;
	private boolean databaseExists = true;

	@RootContext
	Context context;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void init() {
		isVersionConsistent = true;
		databaseExists = checkDataBase();

		if (context != null && !databaseExists) {
			// By calling "getReadableDatabase" method and empty database will
			// be created into the default system path
			// of your application so we are gonna be able to overwrite that
			// database
			getReadableDatabase();
			databaseExists = FileHelper.copyDatabaseFromAssets(context);
		}

		getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
	}

	private boolean checkDataBase() {
		boolean success = false;
		SQLiteDatabase checkDB = null;
		File databaseFile = context.getDatabasePath(DatabaseHelper.DATABASE_NAME);

		if (databaseFile == null || !databaseFile.exists()) {
			return false;
		}

		try {
			checkDB = SQLiteDatabase.openDatabase(databaseFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);
			
			final Cursor c = checkDB.rawQuery(String.format("SELECT COUNT(1) FROM %s", User.NAME), null);

			if (c.moveToFirst()) {
				success = true;
			}

			c.close();
		} catch (SQLiteException e) {
			success = false;
		} finally {
			if (checkDB != null) {
				checkDB.close();
			}
		}

		return success;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		if (oldVersion != 0 && oldVersion < newVersion) {
			isVersionConsistent = false;
		}
	}

	public boolean isVersionConsistent() {
		return isVersionConsistent;
	}

	public boolean databaseExists() {
		return databaseExists;
	}
}
