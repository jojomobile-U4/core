package com.u4.mobile.model;

import java.util.UUID;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.u4.mobile.base.app.ApplicationEnums.SyncOperation;

public class UploadTransferObjectBase extends DownloadTransferObjectBase {
	public transient static final String SYNC_VERSION_FIELD_NAME = "SyncVersion";
	public transient static final String SYNC_OPERATION_FIELD_NAME = "SyncOperation";

	private static final long serialVersionUID = 1L;

	@SerializedName(SYNC_VERSION_FIELD_NAME)
	@DatabaseField(columnName = SYNC_VERSION_FIELD_NAME, canBeNull = false, defaultValue = "0")
	private String syncVersion;

	@SerializedName(SYNC_OPERATION_FIELD_NAME)
	private SyncOperation syncOperation;

	public UploadTransferObjectBase() {
		super();
	}

	public UploadTransferObjectBase(UploadTransferObjectBase transferObject) {
		super();

		setGuid(UUID.randomUUID().toString());
		setRecordState(transferObject.getRecordState());
		setSyncVersion(transferObject.getSyncVersion());

		switch (transferObject.getRecordState()) {
		case Inserted:
		case Updated:
			setSyncOperation(SyncOperation.C);
			break;
		case Deleted:
			setSyncOperation(SyncOperation.D);
			break;
		default:
			return;
		}
	}

	public SyncOperation getSyncOperation() {
		return syncOperation;
	}

	public String getSyncVersion() {
		return syncVersion;
	}

	public void setSyncOperation(SyncOperation syncOperation) {
		this.syncOperation = syncOperation;
	}

	public void setSyncVersion(String syncVersion) {
		this.syncVersion = syncVersion;
	}
}
