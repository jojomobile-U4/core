package com.u4.mobile.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "StringResource")
public class StringResource extends InternalStructureBase<DownloadTransferObjectBase, Long> {
	public static final String ID = "Id";
	public static final String CODE__FIELD_NAME = "Code";
	public static final String LCID__FIELD_NAME = "Lcid";
	public static final String RESOURCE_VALUE__FIELD_NAME = "ResourceValue";
	public static final String NAME = "StringResource";

	private static final long serialVersionUID = 1L;

	@DatabaseField(columnName = ID, canBeNull = false, generatedId = true)
	private int id;

	@DatabaseField(columnName = LCID__FIELD_NAME, canBeNull = false)
	private int lcid;

	@DatabaseField(columnName = CODE__FIELD_NAME, canBeNull = false)
	private String code;

	@DatabaseField(columnName = RESOURCE_VALUE__FIELD_NAME, canBeNull = false)
	private String resourceValue;

	@Override
	public String getKeyColumnName() {
		return ID;
	}
}
